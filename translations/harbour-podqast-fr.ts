<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/pages/About.qml" line="9"/>
        <source>../data/about.txt</source>
        <translation>../data/about-fr.txt</translation>
    </message>
</context>
<context>
    <name>AppMenu</name>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="9"/>
        <source>Discover</source>
        <translation>Découvrir</translation>
    </message>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="17"/>
        <source>Library</source>
        <translation>Bibliothèque</translation>
    </message>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="33"/>
        <source>Playlist</source>
        <translation>Playlist</translation>
    </message>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="25"/>
        <source>Inbox</source>
        <translation>Boîte aux lettres</translation>
    </message>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="48"/>
        <source>refreshing: </source>
        <translation>Actualisation : </translation>
    </message>
</context>
<context>
    <name>Archive</name>
    <message>
        <location filename="../qml/pages/Archive.qml" line="60"/>
        <source>Library</source>
        <translation>Bibliothèque</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="104"/>
        <source>History</source>
        <translation>Historique</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="111"/>
        <source>Favorites</source>
        <translation>Favoris</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="119"/>
        <source>External Audio</source>
        <translation>Fichiers Audios Externes</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="142"/>
        <source>Rendering</source>
        <translation>Chargement</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="168"/>
        <source>Collecting Podcasts</source>
        <translation>Récupération des Podcasts</translation>
    </message>
</context>
<context>
    <name>ArchivePostListItem</name>
    <message>
        <location filename="../qml/components/ArchivePostListItem.qml" line="75"/>
        <source>Select chapters</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Chapters</name>
    <message>
        <location filename="../qml/pages/Chapters.qml" line="48"/>
        <source>Chapters</source>
        <translation>Chapitres</translation>
    </message>
    <message>
        <location filename="../qml/pages/Chapters.qml" line="92"/>
        <source>No chapters</source>
        <translation>Aucun chapitre</translation>
    </message>
    <message>
        <location filename="../qml/pages/Chapters.qml" line="93"/>
        <source>Rendering chapters</source>
        <translation>Génération des chapitres</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="37"/>
        <source> chapters</source>
        <translation> chapitres</translation>
    </message>
</context>
<context>
    <name>Discover</name>
    <message>
        <location filename="../qml/pages/Discover.qml" line="28"/>
        <source>Discover</source>
        <translation>Découvrir</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="43"/>
        <source>Search on gpodder.net</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="47"/>
        <source>Search on fyyd.de</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="51"/>
        <source>Tags...</source>
        <translation>Tags...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="55"/>
        <source>Url...</source>
        <translation>Url...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="59"/>
        <source>Import...</source>
        <translation>Importer...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="63"/>
        <source>Export...</source>
        <translation>Exporter...</translation>
    </message>
</context>
<context>
    <name>DiscoverExport</name>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="25"/>
        <source>Backup done</source>
        <translation>Sauvegarde terminée</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="26"/>
        <location filename="../qml/pages/DiscoverExport.qml" line="27"/>
        <source>Backup done to </source>
        <translation>Sauvegarde réalisée dans </translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="33"/>
        <source>OPML file saved</source>
        <translation>Fichier OPML sauvegardé</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="34"/>
        <location filename="../qml/pages/DiscoverExport.qml" line="35"/>
        <source>OPML file saved to </source>
        <translation>Fichier OPML sauvegardé dans </translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="53"/>
        <source>Export</source>
        <translation>Exporter</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="64"/>
        <source>Backup</source>
        <translation>Faire une sauvegarde</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="67"/>
        <source>Start Backup</source>
        <translation>Démarrage de la sauvegarde dans </translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="85"/>
        <source>Save to OPML</source>
        <translation>Sauvegarder au format OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="88"/>
        <source>Start OPML export</source>
        <translation>Démarrage de l&apos;export OPML dans </translation>
    </message>
</context>
<context>
    <name>DiscoverFyyd</name>
    <message>
        <location filename="../qml/pages/DiscoverFyyd.qml" line="36"/>
        <source>Discover on fyyd.de</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverFyyd.qml" line="43"/>
        <source>Keyword Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverFyyd.qml" line="47"/>
        <source>Hottest Podcasts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverFyyd.qml" line="51"/>
        <source>Browse Categories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverFyyd.qml" line="55"/>
        <source>Browse Curations</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DiscoverImport</name>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="25"/>
        <source> Podcasts imported</source>
        <translation>Podcasts importés</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="26"/>
        <location filename="../qml/pages/DiscoverImport.qml" line="27"/>
        <source> Podcasts imported from OPML</source>
        <translation>Podcasts importés au format OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="46"/>
        <source>Discover by Importing</source>
        <translation>Découvrir en Important</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="59"/>
        <source>Pick an OPML file</source>
        <translation>Choisissez un fichier OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="60"/>
        <source>OPML file</source>
        <translation>Fichier OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="80"/>
        <source>Import OPML</source>
        <translation>Importer de l&apos;OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="82"/>
        <source>Import OPML File</source>
        <translation>Importer un fichier OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="100"/>
        <source>Import from Gpodder</source>
        <translation>Importer depuis Gpodder</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="102"/>
        <source>Import Gpodder Database</source>
        <translation>Importer une BDD Gpodder</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="125"/>
        <source>Please note: Importing does take some time!</source>
        <translation>Attention : notez qu&apos;un import peut prendre
        du temps !</translation>
    </message>
</context>
<context>
    <name>DiscoverSearch</name>
    <message>
        <location filename="../qml/pages/DiscoverSearch.qml" line="37"/>
        <source>Discover by Search</source>
        <translation>Découvrir en Recherchant</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverSearch.qml" line="68"/>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
</context>
<context>
    <name>DiscoverTags</name>
    <message>
        <location filename="../qml/pages/DiscoverTags.qml" line="43"/>
        <source>Discover Tags</source>
        <translation>Découvrir par Tags</translation>
    </message>
</context>
<context>
    <name>DiscoverUrl</name>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="28"/>
        <source>Discover by URL</source>
        <translation>Découvrir par URL</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="51"/>
        <source>Enter podcasts&apos; feed url</source>
        <translation>Insérer le flux URL contenant les podcasts</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="68"/>
        <source>Please enter feed urls here - not web page urls</source>
        <translation>N&apos;insérer un lien que vers le flux, non vers le site web</translation>
    </message>
</context>
<context>
    <name>External</name>
    <message>
        <location filename="../qml/pages/External.qml" line="50"/>
        <source>External Audio</source>
        <translation>Fichiers Audios Externes</translation>
    </message>
    <message>
        <location filename="../qml/pages/External.qml" line="84"/>
        <source>Rendering</source>
        <translation>Chargement</translation>
    </message>
    <message>
        <location filename="../qml/pages/External.qml" line="85"/>
        <source>Collecting Posts</source>
        <translation>Récupération des Articles</translation>
    </message>
</context>
<context>
    <name>Favorites</name>
    <message>
        <location filename="../qml/pages/Favorites.qml" line="46"/>
        <source>Favorites</source>
        <translation>Favoris</translation>
    </message>
</context>
<context>
    <name>FeedParserPython</name>
    <message>
        <location filename="../qml/components/FeedParserPython.qml" line="184"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
</context>
<context>
    <name>FyydCategories</name>
    <message>
        <location filename="../qml/pages/FyydCategories.qml" line="45"/>
        <source>Categories</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FyydCurations</name>
    <message>
        <location filename="../qml/pages/FyydCurations.qml" line="44"/>
        <source>Curations</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FyydHottest</name>
    <message>
        <location filename="../qml/pages/FyydHottest.qml" line="62"/>
        <source>Hottest podcasts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FyydHottest.qml" line="68"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FyydSearch</name>
    <message>
        <location filename="../qml/pages/FyydSearch.qml" line="34"/>
        <source>No results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FyydSearch.qml" line="35"/>
        <source>Try searching for something else</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FyydSearch.qml" line="56"/>
        <source>Discover on fyyd.de</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FyydSearch.qml" line="61"/>
        <source>Search</source>
        <translation type="unfinished">Rechercher</translation>
    </message>
    <message>
        <location filename="../qml/pages/FyydSearch.qml" line="90"/>
        <source>Here be podcasts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FyydSearch.qml" line="91"/>
        <source>Enter search term above</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>History</name>
    <message>
        <location filename="../qml/pages/History.qml" line="45"/>
        <source>History</source>
        <translation>Historique</translation>
    </message>
    <message>
        <location filename="../qml/pages/History.qml" line="80"/>
        <source>Rendering</source>
        <translation>Chargement</translation>
    </message>
    <message>
        <location filename="../qml/pages/History.qml" line="81"/>
        <source>Collecting Posts</source>
        <translation>Récupération des Articles</translation>
    </message>
</context>
<context>
    <name>Inbox</name>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="57"/>
        <source>Inbox</source>
        <translation>Boîte aux lettres</translation>
    </message>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="66"/>
        <source>Moving all posts to archive</source>
        <translation>Transfert des courriers aux archives</translation>
    </message>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="106"/>
        <source>No new posts</source>
        <translation>Aucun nouveau courrier</translation>
    </message>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="107"/>
        <source>Pull down to Discover new podcasts, get posts from Library, or play the Playlist</source>
        <translation>Glisser vers le bas pour Découvrir de nouveaux podcasts, obtenir de nouveaux courriers de la Bibliothèque, ou écouter la Playlist</translation>
    </message>
</context>
<context>
    <name>InboxPostListItem</name>
    <message>
        <location filename="../qml/components/InboxPostListItem.qml" line="56"/>
        <source>Select chapters</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Player</name>
    <message>
        <location filename="../qml/pages/Player.qml" line="27"/>
        <source>Audio playrate</source>
        <translation>Vitesse de lecture</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="60"/>
        <source>Sleep timer</source>
        <translation>Minuterie avant coupure</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="60"/>
        <source> running...</source>
        <translation> activée...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="174"/>
        <source> chapters</source>
        <translation> chapitres</translation>
    </message>
</context>
<context>
    <name>Player_BACKUP_109265</name>
    <message>
        <location filename="../qml/pages/Player_BACKUP_109265.qml" line="27"/>
        <source>Audio playrate</source>
        <translation type="unfinished">Vitesse de lecture</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player_BACKUP_109265.qml" line="60"/>
        <source>Sleep timer</source>
        <translation type="unfinished">Minuterie avant coupure</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player_BACKUP_109265.qml" line="60"/>
        <source> running...</source>
        <translation type="unfinished"> activée...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player_BACKUP_109265.qml" line="174"/>
        <source> chapters</source>
        <translation type="unfinished"> chapitres</translation>
    </message>
</context>
<context>
    <name>Player_BASE_109265</name>
    <message>
        <location filename="../qml/pages/Player_BASE_109265.qml" line="27"/>
        <source>Audio playrate</source>
        <translation type="unfinished">Vitesse de lecture</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player_BASE_109265.qml" line="62"/>
        <source>Sleep timer</source>
        <translation type="unfinished">Minuterie avant coupure</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player_BASE_109265.qml" line="62"/>
        <source> running...</source>
        <translation type="unfinished"> activée...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player_BASE_109265.qml" line="179"/>
        <source> chapters</source>
        <translation type="unfinished"> chapitres</translation>
    </message>
</context>
<context>
    <name>Player_LOCAL_109265</name>
    <message>
        <location filename="../qml/pages/Player_LOCAL_109265.qml" line="27"/>
        <source>Audio playrate</source>
        <translation type="unfinished">Vitesse de lecture</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player_LOCAL_109265.qml" line="60"/>
        <source>Sleep timer</source>
        <translation type="unfinished">Minuterie avant coupure</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player_LOCAL_109265.qml" line="60"/>
        <source> running...</source>
        <translation type="unfinished"> activée...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player_LOCAL_109265.qml" line="174"/>
        <source> chapters</source>
        <translation type="unfinished"> chapitres</translation>
    </message>
</context>
<context>
    <name>Player_REMOTE_109265</name>
    <message>
        <location filename="../qml/pages/Player_REMOTE_109265.qml" line="27"/>
        <source>Audio playrate</source>
        <translation type="unfinished">Vitesse de lecture</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player_REMOTE_109265.qml" line="62"/>
        <source>Sleep timer</source>
        <translation type="unfinished">Minuterie avant coupure</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player_REMOTE_109265.qml" line="62"/>
        <source> running...</source>
        <translation type="unfinished"> activée...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player_REMOTE_109265.qml" line="179"/>
        <source> chapters</source>
        <translation type="unfinished"> chapitres</translation>
    </message>
</context>
<context>
    <name>Podcast</name>
    <message>
        <location filename="../qml/pages/Podcast.qml" line="173"/>
        <source>Subscribe</source>
        <translation>S&apos;abonner</translation>
    </message>
    <message>
        <location filename="../qml/pages/Podcast.qml" line="173"/>
        <source>Configure</source>
        <translation>Configurer</translation>
    </message>
    <message>
        <location filename="../qml/pages/Podcast.qml" line="210"/>
        <source>Feed alternatives</source>
        <translation>Flux Alternatifs</translation>
    </message>
    <message>
        <location filename="../qml/pages/Podcast.qml" line="230"/>
        <source>Latest Post</source>
        <translation>Derniers Articles</translation>
    </message>
    <message>
        <location filename="../qml/pages/Podcast.qml" line="254"/>
        <source>First Post</source>
        <translation>Premier Article</translation>
    </message>
</context>
<context>
    <name>PodcastEntryItem</name>
    <message>
        <location filename="../qml/components/PodcastEntryItem.qml" line="49"/>
        <source>Select chapters</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PodcastItem</name>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="21"/>
        <source>Refresh</source>
        <translation>Rafraichir</translation>
    </message>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="29"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="37"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="45"/>
        <source>Deleted Podcast</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PodcastSettings</name>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="25"/>
        <source>Posts</source>
        <translation>Articles</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="34"/>
        <source>Move new post to</source>
        <translation>Transférer les nouveaux articles</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="36"/>
        <source>Inbox</source>
        <translation>Dans la Boîte aux lettres</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="37"/>
        <source>Top of Playlist</source>
        <translation>En début de Playlist</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="38"/>
        <source>Bottom of Playlist</source>
        <translation>En fin de playlist</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="39"/>
        <source>Archive</source>
        <translation>Aux Archives</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="46"/>
        <source>Automatic post limit</source>
        <translation>Limite automatique du nombre d&apos;articles</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="63"/>
        <source>Audio playrate</source>
        <translation>Vitesse de lecture</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="72"/>
        <source>Podcast</source>
        <translation>Podcast</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="80"/>
        <source>Subscribe</source>
        <translation>S&apos;inscrire</translation>
    </message>
</context>
<context>
    <name>Podcastsearch</name>
    <message>
        <location filename="../qml/pages/Podcastsearch.qml" line="31"/>
        <source>Search by Tag...</source>
        <translation>Rechercher par Tag...</translation>
    </message>
</context>
<context>
    <name>PodpostList</name>
    <message>
        <location filename="../qml/pages/PodpostList.qml" line="98"/>
        <source>Rendering</source>
        <translation>Chargement</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodpostList.qml" line="124"/>
        <source>Creating items</source>
        <translation>Génération des éléments</translation>
    </message>
</context>
<context>
    <name>PodpostPostListItem</name>
    <message>
        <location filename="../qml/components/PodpostPostListItem.qml" line="82"/>
        <source>Select chapters</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PostDescription</name>
    <message>
        <location filename="../qml/pages/PostDescription.qml" line="25"/>
        <source>Open in browser</source>
        <translation>Ouvrir dans le navigateur</translation>
    </message>
</context>
<context>
    <name>PrefAboutMenu</name>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="8"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="12"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="16"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
</context>
<context>
    <name>Queue</name>
    <message>
        <location filename="../qml/pages/Queue.qml" line="66"/>
        <source>Playlist</source>
        <translation>File d&apos;attente</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="81"/>
        <source>No posts</source>
        <translation>Aucun article</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="82"/>
        <source>Pull down to Discover new podcasts or get posts from Inbox or Library</source>
        <translation>Glisser vers le bas pour Découvrir de nouveaux podcasts ou obtenir de nouveaux articles de la Boîte aux lettres ou de la Bibliothèque</translation>
    </message>
</context>
<context>
    <name>QueuePostListItem</name>
    <message>
        <location filename="../qml/components/QueuePostListItem.qml" line="56"/>
        <source>Favorite</source>
        <translation>Favoris</translation>
    </message>
    <message>
        <location filename="../qml/components/QueuePostListItem.qml" line="72"/>
        <source>Move up</source>
        <translation>Monter</translation>
    </message>
    <message>
        <location filename="../qml/components/QueuePostListItem.qml" line="84"/>
        <source>Move down</source>
        <translation>Descendre</translation>
    </message>
    <message>
        <location filename="../qml/components/QueuePostListItem.qml" line="96"/>
        <source>Select chapters</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/pages/Settings.qml" line="32"/>
        <source>Global Podcast Settings</source>
        <translation>Paramètres Globaux des Podcasts</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="45"/>
        <source>Move new post to</source>
        <translation>Transférer les nouveaux articles</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="47"/>
        <location filename="../qml/pages/Settings.qml" line="168"/>
        <source>Inbox</source>
        <translation>Boîte aux lettres</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="50"/>
        <location filename="../qml/pages/Settings.qml" line="171"/>
        <source>Archive</source>
        <translation>Archives</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="61"/>
        <source>Automatic post limit</source>
        <translation>Limite automatique du nombre d&apos;articles</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="103"/>
        <source>Audio playrate</source>
        <translation>Vitesse de lecture</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="91"/>
        <source>off</source>
        <translation>Désactivé</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="181"/>
        <source>Download/Streaming</source>
        <translation>Téléchargement/Streaming</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="272"/>
        <source>Development</source>
        <translation>Développement</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="283"/>
        <source>Experimental features</source>
        <translation>Fonctionnalités expérimentales</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="48"/>
        <location filename="../qml/pages/Settings.qml" line="169"/>
        <source>Top of Playlist</source>
        <translation>En début de Playlist</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="49"/>
        <location filename="../qml/pages/Settings.qml" line="170"/>
        <source>Bottom of Playlist</source>
        <translation>En fin de la Playlist</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="87"/>
        <source>Refresh interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="119"/>
        <source>Sleep timer</source>
        <translation>Minuterie avant coupure</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="123"/>
        <source>min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="133"/>
        <source>External Audio</source>
        <translation>Fichiers Audios Externes</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="145"/>
        <source>Allow external audio</source>
        <translation>Autoriser les fichiers externes</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="153"/>
        <source>Will take data from
</source>
        <translation>Prendra les données depuis
</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="166"/>
        <source>Move external audio to</source>
        <translation>Transférer les fichiers externes</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="198"/>
        <source>Download Playlist Posts</source>
        <translation>Télécharger les Playlists d&apos;Articles</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="207"/>
        <source>Download on Mobile</source>
        <translation>Télécharger hors Wifi</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="216"/>
        <source>Keep Favorites downloaded</source>
        <translation>Conserver les favoris déjà téléchargés</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="225"/>
        <source>Will save data in
</source>
        <translation>Sauvegardera les données dans
</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="253"/>
        <source>System</source>
        <translation>Système</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="264"/>
        <source>Audio viewable in system</source>
        <translation>Audio visible dans le système</translation>
    </message>
</context>
<context>
    <name>Wizzard1</name>
    <message>
        <location filename="../qml/pages/Wizzard1.qml" line="30"/>
        <source>../data/about.txt</source>
        <translation>../data/about.txt</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard1.qml" line="48"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
</context>
<context>
    <name>Wizzard2</name>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="36"/>
        <source>Let&apos;s start...</source>
        <translation>Commençons...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="37"/>
        <source>Back to info</source>
        <translation>Retourner aux informations</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="54"/>
        <source> Podcasts imported</source>
        <translation> Podcasts importés</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="55"/>
        <location filename="../qml/pages/Wizzard2.qml" line="56"/>
        <source> Podcasts imported from OPML</source>
        <translation> Podcasts importés de l&apos;OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="73"/>
        <source>You are now able to import old stuff. You can import from Discover lateron. Please note: Importing does take some time!</source>
        <translation>Vous ne pouvez plus importer de vieux trucs. Vous pouvez cependant importer depuis Découvrir dorénavant. Notez que l&apos;import peut prendre du temps !</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="102"/>
        <source>Pick an OPML file</source>
        <translation>Choisissez un fichier OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="103"/>
        <source>OPML file</source>
        <translation>Fichier OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="125"/>
        <source>Import OPML</source>
        <translation>Importer de l&apos;OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="127"/>
        <source>Import OPML File</source>
        <translation>Importer un fichier OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="146"/>
        <source>Import from Gpodder</source>
        <translation>Importer depuis Gpodder</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="149"/>
        <source>Import Gpodder Database</source>
        <translation>Importer une base de données Gpodder</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="175"/>
        <source>When you now start you can discover new podcasts. After that you can for example select new posts to play from the Library.</source>
        <translation>Vous pouvez maintenant découvrir de nouveaux podcasts. Vous pourrez ensuite les écouter depuis la Bibliothèque.</translation>
    </message>
</context>
<context>
    <name>harbour-podqast</name>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="322"/>
        <source>New posts available</source>
        <translation>Nouveaux articles disponibles</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="323"/>
        <source>Click to view updates</source>
        <translation>Cliquez pour voir les mises à jour</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="324"/>
        <source>New Posts are available. Click to view.</source>
        <translation>De nouveaux articles sont diponibles. Cliquez pour voir.</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="341"/>
        <location filename="../qml/harbour-podqast.qml" line="342"/>
        <source>PodQast message</source>
        <translation>Message PodQast</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="343"/>
        <source>New PodQast message</source>
        <translation>Nouveau message PodQast</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="370"/>
        <source> Podcasts imported</source>
        <translation> Podcasts importés</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="371"/>
        <location filename="../qml/harbour-podqast.qml" line="372"/>
        <source> Podcasts imported from OPML</source>
        <translation> Podcasts importés depuis OPML</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="469"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="470"/>
        <location filename="../qml/harbour-podqast.qml" line="471"/>
        <source>Audio File not existing</source>
        <translation>Le fichier audio n&apos;existe pas</translation>
    </message>
</context>
</TS>
