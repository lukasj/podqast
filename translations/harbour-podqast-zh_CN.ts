<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/pages/About.qml" line="9"/>
        <source>../data/about.txt</source>
        <translation>关于</translation>
    </message>
</context>
<context>
    <name>AppMenu</name>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="9"/>
        <source>Discover</source>
        <translation>发现</translation>
    </message>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="17"/>
        <source>Library</source>
        <translation>库</translation>
    </message>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="33"/>
        <source>Playlist</source>
        <translation>播放列表</translation>
    </message>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="25"/>
        <source>Inbox</source>
        <translation>收件箱</translation>
    </message>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="48"/>
        <source>refreshing: </source>
        <translation>刷新：</translation>
    </message>
</context>
<context>
    <name>Archive</name>
    <message>
        <location filename="../qml/pages/Archive.qml" line="60"/>
        <source>Library</source>
        <translation>库</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="104"/>
        <source>History</source>
        <translation>历史</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="111"/>
        <source>Favorites</source>
        <translation>收藏</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="119"/>
        <source>External Audio</source>
        <translation>外部音频</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="142"/>
        <source>Rendering</source>
        <translation>更新</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="168"/>
        <source>Collecting Podcasts</source>
        <translation>收藏的播客</translation>
    </message>
</context>
<context>
    <name>ArchivePostListItem</name>
    <message>
        <location filename="../qml/components/ArchivePostListItem.qml" line="75"/>
        <source>Select chapters</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Chapters</name>
    <message>
        <location filename="../qml/pages/Chapters.qml" line="48"/>
        <source>Chapters</source>
        <translation>章节</translation>
    </message>
    <message>
        <location filename="../qml/pages/Chapters.qml" line="92"/>
        <source>No chapters</source>
        <translation>无章节</translation>
    </message>
    <message>
        <location filename="../qml/pages/Chapters.qml" line="93"/>
        <source>Rendering chapters</source>
        <translation>更新章节</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="37"/>
        <source> chapters</source>
        <translation>章节</translation>
    </message>
</context>
<context>
    <name>Discover</name>
    <message>
        <location filename="../qml/pages/Discover.qml" line="28"/>
        <source>Discover</source>
        <translation>发现</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="43"/>
        <source>Search on gpodder.net</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="47"/>
        <source>Search on fyyd.de</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="51"/>
        <source>Tags...</source>
        <translation>标签……</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="55"/>
        <source>Url...</source>
        <translation>链接……</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="59"/>
        <source>Import...</source>
        <translation>导入……</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="63"/>
        <source>Export...</source>
        <translation>导出……</translation>
    </message>
</context>
<context>
    <name>DiscoverExport</name>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="25"/>
        <source>Backup done</source>
        <translation>备份完成</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="26"/>
        <location filename="../qml/pages/DiscoverExport.qml" line="27"/>
        <source>Backup done to </source>
        <translation>备份完成</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="33"/>
        <source>OPML file saved</source>
        <translation>已保存 OPML 文件</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="34"/>
        <location filename="../qml/pages/DiscoverExport.qml" line="35"/>
        <source>OPML file saved to </source>
        <translation>OPML 文件保存到</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="53"/>
        <source>Export</source>
        <translation>导出</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="64"/>
        <source>Backup</source>
        <translation>备份</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="67"/>
        <source>Start Backup</source>
        <translation>开始备份</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="85"/>
        <source>Save to OPML</source>
        <translation>保存到 OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="88"/>
        <source>Start OPML export</source>
        <translation>开始导出 OPML </translation>
    </message>
</context>
<context>
    <name>DiscoverFyyd</name>
    <message>
        <location filename="../qml/pages/DiscoverFyyd.qml" line="36"/>
        <source>Discover on fyyd.de</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverFyyd.qml" line="43"/>
        <source>Keyword Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverFyyd.qml" line="47"/>
        <source>Hottest Podcasts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverFyyd.qml" line="51"/>
        <source>Browse Categories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverFyyd.qml" line="55"/>
        <source>Browse Curations</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DiscoverImport</name>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="25"/>
        <source> Podcasts imported</source>
        <translation>已导出播客</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="26"/>
        <location filename="../qml/pages/DiscoverImport.qml" line="27"/>
        <source> Podcasts imported from OPML</source>
        <translation>播客导出自 OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="46"/>
        <source>Discover by Importing</source>
        <translation>导出发现</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="59"/>
        <source>Pick an OPML file</source>
        <translation>选择 OPML 文件</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="60"/>
        <source>OPML file</source>
        <translation>OPML 文件</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="80"/>
        <source>Import OPML</source>
        <translation>导出 OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="82"/>
        <source>Import OPML File</source>
        <translation>导出 OPML 文件</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="100"/>
        <source>Import from Gpodder</source>
        <translation>从 Gpodder 导入</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="102"/>
        <source>Import Gpodder Database</source>
        <translation>导入 Gpodder 数据库</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="125"/>
        <source>Please note: Importing does take some time!</source>
        <translation>请注意：导入将会需要一些时间!</translation>
    </message>
</context>
<context>
    <name>DiscoverSearch</name>
    <message>
        <location filename="../qml/pages/DiscoverSearch.qml" line="37"/>
        <source>Discover by Search</source>
        <translation>通过搜索找到</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverSearch.qml" line="68"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
</context>
<context>
    <name>DiscoverTags</name>
    <message>
        <location filename="../qml/pages/DiscoverTags.qml" line="43"/>
        <source>Discover Tags</source>
        <translation>找到标签</translation>
    </message>
</context>
<context>
    <name>DiscoverUrl</name>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="28"/>
        <source>Discover by URL</source>
        <translation>通过链接找到</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="51"/>
        <source>Enter podcasts&apos; feed url</source>
        <translation>输入播客及订阅链接</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="68"/>
        <source>Please enter feed urls here - not web page urls</source>
        <translation>请在此输入订阅链接而非网页链接</translation>
    </message>
</context>
<context>
    <name>External</name>
    <message>
        <location filename="../qml/pages/External.qml" line="50"/>
        <source>External Audio</source>
        <translation>外部音频</translation>
    </message>
    <message>
        <location filename="../qml/pages/External.qml" line="84"/>
        <source>Rendering</source>
        <translation>更新</translation>
    </message>
    <message>
        <location filename="../qml/pages/External.qml" line="85"/>
        <source>Collecting Posts</source>
        <translation>收藏内容</translation>
    </message>
</context>
<context>
    <name>Favorites</name>
    <message>
        <location filename="../qml/pages/Favorites.qml" line="46"/>
        <source>Favorites</source>
        <translation>收藏</translation>
    </message>
</context>
<context>
    <name>FeedParserPython</name>
    <message>
        <location filename="../qml/components/FeedParserPython.qml" line="184"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
</context>
<context>
    <name>FyydCategories</name>
    <message>
        <location filename="../qml/pages/FyydCategories.qml" line="45"/>
        <source>Categories</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FyydCurations</name>
    <message>
        <location filename="../qml/pages/FyydCurations.qml" line="44"/>
        <source>Curations</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FyydHottest</name>
    <message>
        <location filename="../qml/pages/FyydHottest.qml" line="62"/>
        <source>Hottest podcasts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FyydHottest.qml" line="68"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FyydSearch</name>
    <message>
        <location filename="../qml/pages/FyydSearch.qml" line="34"/>
        <source>No results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FyydSearch.qml" line="35"/>
        <source>Try searching for something else</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FyydSearch.qml" line="56"/>
        <source>Discover on fyyd.de</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FyydSearch.qml" line="61"/>
        <source>Search</source>
        <translation type="unfinished">搜索</translation>
    </message>
    <message>
        <location filename="../qml/pages/FyydSearch.qml" line="90"/>
        <source>Here be podcasts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FyydSearch.qml" line="91"/>
        <source>Enter search term above</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>History</name>
    <message>
        <location filename="../qml/pages/History.qml" line="45"/>
        <source>History</source>
        <translation>历史</translation>
    </message>
    <message>
        <location filename="../qml/pages/History.qml" line="80"/>
        <source>Rendering</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/History.qml" line="81"/>
        <source>Collecting Posts</source>
        <translation>收藏内容</translation>
    </message>
</context>
<context>
    <name>Inbox</name>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="57"/>
        <source>Inbox</source>
        <translation>收件箱</translation>
    </message>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="66"/>
        <source>Moving all posts to archive</source>
        <translation>移动所有内容到存档</translation>
    </message>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="106"/>
        <source>No new posts</source>
        <translation>无新内容</translation>
    </message>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="107"/>
        <source>Pull down to Discover new podcasts, get posts from Library, or play the Playlist</source>
        <translation>下拉以找到新播客、从库获取内容或播放播放列表中的内容</translation>
    </message>
</context>
<context>
    <name>InboxPostListItem</name>
    <message>
        <location filename="../qml/components/InboxPostListItem.qml" line="56"/>
        <source>Select chapters</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Player</name>
    <message>
        <location filename="../qml/pages/Player.qml" line="27"/>
        <source>Audio playrate</source>
        <translation>音频播放器</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="60"/>
        <source>Sleep timer</source>
        <translation>睡眠计时器</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="60"/>
        <source> running...</source>
        <translation>运行中……</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="174"/>
        <source> chapters</source>
        <translation>章节</translation>
    </message>
</context>
<context>
    <name>Player_BACKUP_109265</name>
    <message>
        <location filename="../qml/pages/Player_BACKUP_109265.qml" line="27"/>
        <source>Audio playrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Player_BACKUP_109265.qml" line="60"/>
        <source>Sleep timer</source>
        <translation type="unfinished">睡眠计时器</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player_BACKUP_109265.qml" line="60"/>
        <source> running...</source>
        <translation type="unfinished">运行中……</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player_BACKUP_109265.qml" line="174"/>
        <source> chapters</source>
        <translation type="unfinished">章节</translation>
    </message>
</context>
<context>
    <name>Player_BASE_109265</name>
    <message>
        <location filename="../qml/pages/Player_BASE_109265.qml" line="27"/>
        <source>Audio playrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Player_BASE_109265.qml" line="62"/>
        <source>Sleep timer</source>
        <translation type="unfinished">睡眠计时器</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player_BASE_109265.qml" line="62"/>
        <source> running...</source>
        <translation type="unfinished">运行中……</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player_BASE_109265.qml" line="179"/>
        <source> chapters</source>
        <translation type="unfinished">章节</translation>
    </message>
</context>
<context>
    <name>Player_LOCAL_109265</name>
    <message>
        <location filename="../qml/pages/Player_LOCAL_109265.qml" line="27"/>
        <source>Audio playrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Player_LOCAL_109265.qml" line="60"/>
        <source>Sleep timer</source>
        <translation type="unfinished">睡眠计时器</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player_LOCAL_109265.qml" line="60"/>
        <source> running...</source>
        <translation type="unfinished">运行中……</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player_LOCAL_109265.qml" line="174"/>
        <source> chapters</source>
        <translation type="unfinished">章节</translation>
    </message>
</context>
<context>
    <name>Player_REMOTE_109265</name>
    <message>
        <location filename="../qml/pages/Player_REMOTE_109265.qml" line="27"/>
        <source>Audio playrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Player_REMOTE_109265.qml" line="62"/>
        <source>Sleep timer</source>
        <translation type="unfinished">睡眠计时器</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player_REMOTE_109265.qml" line="62"/>
        <source> running...</source>
        <translation type="unfinished">运行中……</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player_REMOTE_109265.qml" line="179"/>
        <source> chapters</source>
        <translation type="unfinished">章节</translation>
    </message>
</context>
<context>
    <name>Podcast</name>
    <message>
        <location filename="../qml/pages/Podcast.qml" line="173"/>
        <source>Subscribe</source>
        <translation>订阅</translation>
    </message>
    <message>
        <location filename="../qml/pages/Podcast.qml" line="173"/>
        <source>Configure</source>
        <translation>配置</translation>
    </message>
    <message>
        <location filename="../qml/pages/Podcast.qml" line="210"/>
        <source>Feed alternatives</source>
        <translation>可选订阅溜</translation>
    </message>
    <message>
        <location filename="../qml/pages/Podcast.qml" line="230"/>
        <source>Latest Post</source>
        <translation>最新的内容</translation>
    </message>
    <message>
        <location filename="../qml/pages/Podcast.qml" line="254"/>
        <source>First Post</source>
        <translation>最早的内容</translation>
    </message>
</context>
<context>
    <name>PodcastEntryItem</name>
    <message>
        <location filename="../qml/components/PodcastEntryItem.qml" line="49"/>
        <source>Select chapters</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PodcastItem</name>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="21"/>
        <source>Refresh</source>
        <translation>刷新&gt;</translation>
    </message>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="29"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="37"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="45"/>
        <source>Deleted Podcast</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PodcastSettings</name>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="25"/>
        <source>Posts</source>
        <translation>内容</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="34"/>
        <source>Move new post to</source>
        <translation>移动新内容到</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="36"/>
        <source>Inbox</source>
        <translation>收件箱</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="37"/>
        <source>Top of Playlist</source>
        <translation>播放列表顶部</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="38"/>
        <source>Bottom of Playlist</source>
        <translation>播放列表底部</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="39"/>
        <source>Archive</source>
        <translation>存档</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="46"/>
        <source>Automatic post limit</source>
        <translation>自动限制内容</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="63"/>
        <source>Audio playrate</source>
        <translation>音频播放率</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="72"/>
        <source>Podcast</source>
        <translation>播客</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="80"/>
        <source>Subscribe</source>
        <translation>订阅</translation>
    </message>
</context>
<context>
    <name>Podcastsearch</name>
    <message>
        <location filename="../qml/pages/Podcastsearch.qml" line="31"/>
        <source>Search by Tag...</source>
        <translation>按标签搜索……</translation>
    </message>
</context>
<context>
    <name>PodpostList</name>
    <message>
        <location filename="../qml/pages/PodpostList.qml" line="98"/>
        <source>Rendering</source>
        <translation>更新</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodpostList.qml" line="124"/>
        <source>Creating items</source>
        <translation>创建新项目</translation>
    </message>
</context>
<context>
    <name>PodpostPostListItem</name>
    <message>
        <location filename="../qml/components/PodpostPostListItem.qml" line="82"/>
        <source>Select chapters</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PostDescription</name>
    <message>
        <location filename="../qml/pages/PostDescription.qml" line="25"/>
        <source>Open in browser</source>
        <translation>用浏览器打开</translation>
    </message>
</context>
<context>
    <name>PrefAboutMenu</name>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="8"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="12"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="16"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
</context>
<context>
    <name>Queue</name>
    <message>
        <location filename="../qml/pages/Queue.qml" line="66"/>
        <source>Playlist</source>
        <translation>播放列表</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="81"/>
        <source>No posts</source>
        <translation>无内容</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="82"/>
        <source>Pull down to Discover new podcasts or get posts from Inbox or Library</source>
        <translation>下拉以找到新的播客或从收件箱及库获取内容</translation>
    </message>
</context>
<context>
    <name>QueuePostListItem</name>
    <message>
        <location filename="../qml/components/QueuePostListItem.qml" line="56"/>
        <source>Favorite</source>
        <translation>收藏</translation>
    </message>
    <message>
        <location filename="../qml/components/QueuePostListItem.qml" line="72"/>
        <source>Move up</source>
        <translation>上移</translation>
    </message>
    <message>
        <location filename="../qml/components/QueuePostListItem.qml" line="84"/>
        <source>Move down</source>
        <translation>下移</translation>
    </message>
    <message>
        <location filename="../qml/components/QueuePostListItem.qml" line="96"/>
        <source>Select chapters</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/pages/Settings.qml" line="32"/>
        <source>Global Podcast Settings</source>
        <translation>全局播客设置</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="45"/>
        <source>Move new post to</source>
        <translation>移动新内容到</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="47"/>
        <location filename="../qml/pages/Settings.qml" line="168"/>
        <source>Inbox</source>
        <translation>收件箱</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="50"/>
        <location filename="../qml/pages/Settings.qml" line="171"/>
        <source>Archive</source>
        <translation>存档</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="61"/>
        <source>Automatic post limit</source>
        <translation>自动限制内容</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="103"/>
        <source>Audio playrate</source>
        <translation>音频比率</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="91"/>
        <source>off</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="181"/>
        <source>Download/Streaming</source>
        <translation>下载</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="272"/>
        <source>Development</source>
        <translation>开发</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="283"/>
        <source>Experimental features</source>
        <translation>实验功能</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="48"/>
        <location filename="../qml/pages/Settings.qml" line="169"/>
        <source>Top of Playlist</source>
        <translation>播放器顶部</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="49"/>
        <location filename="../qml/pages/Settings.qml" line="170"/>
        <source>Bottom of Playlist</source>
        <translation>播放器底部</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="87"/>
        <source>Refresh interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="119"/>
        <source>Sleep timer</source>
        <translation>睡眠计时器</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="123"/>
        <source>min</source>
        <translation>分钟</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="133"/>
        <source>External Audio</source>
        <translation>外部音频</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="145"/>
        <source>Allow external audio</source>
        <translation>允许外部音频</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="153"/>
        <source>Will take data from
</source>
        <translation>将会获取数据自</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="166"/>
        <source>Move external audio to</source>
        <translation>移动外部音频到</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="198"/>
        <source>Download Playlist Posts</source>
        <translation>下载播放列表内容</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="207"/>
        <source>Download on Mobile</source>
        <translation>在移动设备下载</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="216"/>
        <source>Keep Favorites downloaded</source>
        <translation>保留下载的收藏</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="225"/>
        <source>Will save data in
</source>
        <translation>将会保存数据到</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="253"/>
        <source>System</source>
        <translation>系统</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="264"/>
        <source>Audio viewable in system</source>
        <translation>音频在系统可见</translation>
    </message>
</context>
<context>
    <name>Wizzard1</name>
    <message>
        <location filename="../qml/pages/Wizzard1.qml" line="30"/>
        <source>../data/about.txt</source>
        <translation>向导1</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard1.qml" line="48"/>
        <source>Next</source>
        <translation>下一个</translation>
    </message>
</context>
<context>
    <name>Wizzard2</name>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="36"/>
        <source>Let&apos;s start...</source>
        <translation>向导2</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="37"/>
        <source>Back to info</source>
        <translation>返回到信息</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="54"/>
        <source> Podcasts imported</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="55"/>
        <location filename="../qml/pages/Wizzard2.qml" line="56"/>
        <source> Podcasts imported from OPML</source>
        <translation>已从 OPML 输入播客</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="73"/>
        <source>You are now able to import old stuff. You can import from Discover lateron. Please note: Importing does take some time!</source>
        <translation>你现在可以输入旧的内容。你可以稍后从发现输入。请注意：导入需要一些时间!</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="102"/>
        <source>Pick an OPML file</source>
        <translation>选择 OPML 文件</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="103"/>
        <source>OPML file</source>
        <translation>OPML 文件</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="125"/>
        <source>Import OPML</source>
        <translation>导入 OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="127"/>
        <source>Import OPML File</source>
        <translation>输入 OPML 文件</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="146"/>
        <source>Import from Gpodder</source>
        <translation>从 Gpodder 导入</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="149"/>
        <source>Import Gpodder Database</source>
        <translation>导入 Gpodder 数据库</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="175"/>
        <source>When you now start you can discover new podcasts. After that you can for example select new posts to play from the Library.</source>
        <translation>你现在开始可以寻找新的播客。之后，你可以从库中选择要播放的新内容。</translation>
    </message>
</context>
<context>
    <name>harbour-podqast</name>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="322"/>
        <source>New posts available</source>
        <translation>有可获取的新内容</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="323"/>
        <source>Click to view updates</source>
        <translation>点击以查看更新</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="324"/>
        <source>New Posts are available. Click to view.</source>
        <translation>有可用新内容，点击以查看。</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="341"/>
        <location filename="../qml/harbour-podqast.qml" line="342"/>
        <source>PodQast message</source>
        <translation>PodQast 消息</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="343"/>
        <source>New PodQast message</source>
        <translation>新 PodQast 消息</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="370"/>
        <source> Podcasts imported</source>
        <translation> 播客已导入</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="371"/>
        <location filename="../qml/harbour-podqast.qml" line="372"/>
        <source> Podcasts imported from OPML</source>
        <translation>已从 OPML 导入播客</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="469"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="470"/>
        <location filename="../qml/harbour-podqast.qml" line="471"/>
        <source>Audio File not existing</source>
        <translation>音频文件不存在</translation>
    </message>
</context>
</TS>
