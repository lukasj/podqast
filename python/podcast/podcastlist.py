"""
List of subscribed podcasts
"""

import sys

sys.path.append("../")

from podcast.singleton import Singleton
from podcast.factory import Factory
from podcast.podcast import PodcastFactory
from podcast import util
from podcast import gpodder_import
import pyotherside

listname = "the_podcast_list"


class PodcastList:
    """
    This element holds all podcasts
    """

    def __init__(self):
        """
        Initialization
        """

        self.podcasts = []

    def add(self, url):
        """
        Add a podcast

        podcast: the podcast to be added
        """

        podcast = PodcastFactory().get_podcast(url)

        if podcast:
            if podcast.title:
                podcast.subscribed = True
                podcast.save()
                if url not in self.podcasts:
                    self.podcasts.append(url)
                self.save()

    def delete_podcast(self, url):
        """
        Remove a podcast from list

        podcast: the podcast to be removed
        """

        self.podcasts.remove(url)
        pc = PodcastFactory().get_podcast(url)
        if pc:
            pc.delete()
        self.save()

    def get_podcasts(self):
        """
        return the list of subscribed podcasts
        """

        for podcast in self.podcasts:
            pc = PodcastFactory().get_podcast(podcast)
            if pc:
                try:
                    if pc.subscribed:
                        yield podcast
                except:
                    pyotherside.send("error", "could not get %s." % pc.title)

    def refresh(self, moveto, limit=0):
        """
        Refresh all podcasts and put them in queue, index, or archive
        """

        pllen = len(self.podcasts)
        count = 0
        for podcast in self.podcasts:
            pc = PodcastFactory().get_podcast(podcast)
            if pc:
                try:
                    if pc.subscribed:
                        for post in pc.refresh(moveto, limit):
                            yield post
                except:
                    pyotherside.send(
                        "error", "podcast refresh failed for %s." % pc.title
                    )

            count += 1
            pyotherside.send("refreshProgress", count / pllen)

    def save(self):
        """
        Save the PodcastList
        """

        Factory().get_store().store(listname, self)

    def import_opml(self, opmlfile):
        """
        add podcasts from opml file
        """

        podcounter = 0

        for pod in util.parse_opml(opmlfile):
            pyotherside.send("importing " + pod)
            try:
                self.add(pod)
                podcounter += 1
            except:
                pyotherside.send("apperror", "failed to import " + pod)

        self.save()
        return podcounter

    def import_gpodder(self):
        """
        import podcasts from gpodder database
        TODO: Import the posts
        """

        podcounter = 0

        for pod in gpodder_import.get():
            try:
                self.add(pod)
                podcounter += 1
            except:
                pyotherside.send("apperror", "failed to import " + pod)

        self.save()
        return podcounter


class PodcastListFactory(metaclass=Singleton):
    """
    The Factory of fhe PodcastList
    """

    def __init__(self, progname="harbour-podqast"):
        """
        Initialization
        just initialize pointer to podcastlist
        """

        self.list = None

    def get_podcast_list(self):
        """
        Get the podcast list
        """

        if self.list:
            return self.list

        self.list = Factory().get_store().get(listname)
        if not self.list:
            self.list = PodcastList()
        return self.list
