"""
some utilities
"""

import os
import urllib.request
import urllib.error
import urllib.parse
import time
import datetime
from xml.etree import ElementTree as ET
from xml.etree.ElementTree import Element, SubElement, Comment, tostring
from xml.dom import minidom
import pyotherside
import mutagen
import hashlib
import base64
from mutagen.flac import Picture, error as FLACError

user_agent = "PodQast/2.14 +https://gitlab.com/cy8aer/podqast"
user_agent2 = (
    "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"
)


def make_directory(path):
    """
    Tries to create a directory if it does not exist already
    Return: True if directory exists, False otherwise
    """

    if os.path.isdir(path):
        return True

    try:
        os.makedirs(path, 0o755)
    except:
        return False

    return True


def make_symlink(frompath, topath):
    """
    create a symlink
    """

    if os.path.islink(topath):
        return True

    try:
        os.symlink(frompath, topath)
    except:
        return False

    return True


def tx_to_s(tstring):
    p = tstring.rfind(":")
    if p < 0:
        return int(tstring)
    m = int(tstring[p + 1 :])
    return m + 60 * tx_to_s(tstring[:p])


def s_to_hr(secs):
    hr = ""

    if secs >= 3600:
        hours = int(secs / 3600)
        hr = str(hours) + "h"
        secs = secs - hours * 3600
    print(secs)
    if secs >= 59:
        hr = hr + str(int(secs / 60)) + "m"

    return hr


def s_to_year(secs):
    return time.strftime("%Y-%m-%d", time.gmtime(secs))


def dl_from_url(url, path):
    """
    Download a file from url and save to path
    """

    if url.find("tumblr.com") >= 0:
        agent = user_agent2
    else:
        agent = user_agent

    rsp = urllib.request.Request(url, data=None, headers={"User-Agent": agent})
    response = urllib.request.urlopen(rsp)
    with open(path, "wb") as fhandle:
        fhandle.write(response.read())


def dl_from_url_progress(url, path):
    """
    Download file from url and save to path
    yield percent state
    """

    count = 0
    p1 = 0

    if url.find("tumblr.com") >= 0:
        agent = user_agent2
    else:
        agent = user_agent

    url = urllib.parse.urlsplit(url)
    url = list(url)
    url[2] = urllib.parse.quote(url[2])
    url = urllib.parse.urlunsplit(url)

    req = urllib.request.Request(url, data=None, headers={"User-Agent": agent})
    try:
        h = urllib.request.urlopen(req)
    except urllib.error.HTTPError as e:
        if hasattr(e, "reason"):
            pyotherside.send("apperror", "Error opening URL: " + e.reason)
        return

    length = int(h.getheader("content-length"))

    with open(path, "wb") as fhandle:
        while True:
            chunk = h.read(1024)
            if not chunk:
                break
            count += 1024
            fhandle.write(chunk)
            p2 = int(count / length * 100)
            if p2 > p1:
                p1 = p2
                yield p1


def get_file_length(path):
    """
    Get the file length of path. Used for download status
    """

    return os.path.getsize(path)


def delete_file(path):
    """
    Delete file if exists
    """
    if os.path.exists(path):
        os.remove(path)


def format_date(timestamp):
    """
    Convert a UNIX timestamp to a date representation. From gpodder
    """

    if timestamp is None:
        return None

    seconds_in_a_day = 60 * 60 * 24

    try:
        diff = int((time.time() - timestamp) / seconds_in_a_day)
    except:
        return None

    try:
        timestamp = datetime.datetime.fromtimestamp(timestamp)
    except:
        return None

    if diff < 7:
        return timestamp.strftime("%A")
    else:
        return timestamp.strftime("%x")


def format_full_date(timestamp):
    """
    Get a full date string (monday..., may 2018, 2017, ...)
    """

    now = datetime.datetime.now()
    tnow = time.time()
    diff = tnow - timestamp
    if diff < 60 * 60 * 24 * 7:
        return format_date(timestamp)

    dt = datetime.datetime.fromtimestamp(timestamp)
    if dt.year == now.year:
        return dt.strftime("%B %Y")
    return dt.strftime("%Y")


def parse_opml(filename):
    """
    parse opml file and yield all podcast urls
    """

    tree = None

    try:
        with open(filename, "rt", encoding="utf-8") as f:
            tree = ET.parse(f)
    except:
        pyotherside.send("apperror", "Error importing %s" % filename)
        return

    if tree:
        for node in tree.findall(".//outline"):
            url = node.attrib.get("xmlUrl")
            if url:
                yield url


def prettify(elem):
    """Return a pretty-printed XML string for the Element.
    """
    rough_string = tostring(elem, "utf-8")
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")


def create_opml(filename, data):
    """
    Create opml file from data
    """

    generated_on = str(datetime.datetime.now())

    root = Element("opml")
    root.set("version", "1.0")
    root.append(Comment("Generated by Podqast"))

    head = SubElement(root, "head")
    title = SubElement(head, "title")
    title.text = "PodQasts"
    dc = SubElement(head, "dateCreted")
    dc.text = generated_on
    dm = SubElement(head, "dateModified")
    dm.text = generated_on
    body = SubElement(root, "body")

    for dat in data:
        SubElement(
            body,
            "outline",
            {
                "text": dat["name"],
                "xmlUrl": dat["xml_url"],
                "htmlUrl": dat["html_url"],
            },
        )
    try:
        opmlfile = open(filename, "wb")
        opmlfile.write(prettify(root).encode())
        opmlfile.close()
    except:
        pyotherside.send("error", "Writing opml file failed")
        return

    pyotherside.send("opmlSaveDone", filename)


afiletypes = {
    "<class 'mutagen.mp3.MP3'>": "mp3",
    "<class 'mutagen.mp4.MP4'>": "mp4",
    "<class 'mutagen.oggvorbis.OggVorbis'>": "ogg",
    "<class 'mutagen.flac.FLAC'>": "flac",
    "<class 'mutagen.oggopus.OggOpus'>": "opus",
    "<class 'mutagen.oggspeex.OggSpeex'>": "speex",
    "<class 'mutagen.oggtheora.OggTheora'>": "theora",
    "<class 'mutagen.aac.AAC'>": "aac",
}


def get_audio_meta(audiofile):
    """
    Get audio file metadata
    """

    adata = mutagen.File(audiofile)

    if "title" in adata:
        title = adata["title"][0]
    elif "TIT2" in adata:
        title = adata["TIT2"].text[0]
    else:
        title = audiofile[audiofile.rfind("/") + 1 :]

    pyotherside.send(title)

    if "artist" in adata:
        artist = adata["artist"][0]
    elif "TPE2" in adata:
        artist = adata["TPE2"].text[0]
    else:
        artist = ""

    if "genre" in adata:
        genre = adata["genre"][0]
    elif "TCON" in adata:
        genre = adata["TCON"].text[0]
    else:
        genre = ""

    return {"title": title, "artist": artist, "genre": genre}


def get_audio_tech(audiofile):
    """
    Get technical audio data
    """

    adata = mutagen.File(audiofile)

    try:
        bitrate = adata.info.bitrate
    except:
        bitrate = 0

    length = adata.info.length
    channels = adata.info.channels
    try:
        filetype = afiletypes[str(type(adata))]
    except:
        filetype = "unknown"

    return {
        "bitrate": bitrate,
        "length": length,
        "channels": channels,
        "filetype": filetype,
    }


def down_audio_icon(afile, downpath):
    """
    Download the album art icon to downfile
    """

    filepath = "../../images/audio.png"
    types = {"image/jpeg": "jpg", "image/png": "png", "image/gif": "gif"}

    adata = mutagen.File(afile)

    if "APIC:" in adata:
        image = adata["APIC:"]
        ftype = image.mime
        data = image.data

        ext = types[ftype]
        filename = hashlib.sha256(afile.encode()).hexdigest() + "." + ext
        filepath = os.path.join(downpath, filename)
        try:
            with open(filepath, "wb") as h:
                h.write(data)
        except:
            filepath = "../../images/audio.png"

    if "metadata_block_picture" in adata:
        for b64_data in adata.get("metadata_block_picture", []):
            try:
                data = base64.b64decode(b64_data)
            except (TypeError, ValueError):
                continue

            try:
                picture = Picture(data)
            except FLACError:
                continue

            ext = types.get(picture.mime, "jpg")
            filename = hashlib.sha256(afile.encode()).hexdigest() + "." + ext
            filepath = os.path.join(downpath, filename)
            try:
                with open(filepath, "wb") as h:
                    h.write(picture.data)
            except:
                filepath = "../../images/audio.png"

    return filepath
