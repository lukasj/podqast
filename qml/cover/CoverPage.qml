import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {
    Image {
        id: coverImage
        source: playerHandler.playicon === "" ? "../../images/podcast.png" : playerHandler.playicon
        width: parent.height
        height: parent.height
        anchors.horizontalCenter: parent.horizontalCenter
        opacity: 0.4
    }
    Column {
        id: titlecol
        height: Theme.itemSizeHuge * 0.9
        width: parent.width
        clip: true
        Label {
            height: parent.height
            width: parent.width
            text: playerHandler.firsttitle
            font.bold: true
            font.pixelSize: Theme.fontSizeSmall
            wrapMode: Text.WordWrap
            padding: 10
        }
    }

    Column {
        id: chaptcount
        anchors.top: titlecol.bottom
        width: parent.width
        height: Theme.itemSizeExtraSmall / 1.9
        Label {
            height: parent.height
            width: parent.width
            text: playerHandler.chapters ? playerHandler.chapters.length + qsTr(" chapters") : ""
            font.pixelSize: Theme.fontSizeSmall
            padding: 10
        }
    }

    Column {
        anchors.top: chaptcount.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        Label {
            text: to_pos_str(mediaplayer.position / 1000)
            font.bold: true
            font.pixelSize: Theme.fontSizeLarge
            color: Theme.highlightColor
            anchors.horizontalCenter: parent.horizontalCenter
        }
        Label {
            text: to_pos_str((mediaplayer.duration - mediaplayer.position) / 1000)
            font.bold: true
            font.pixelSize: Theme.fontSizeLarge
            color: Theme.highlightColor
            anchors.horizontalCenter: parent.horizontalCenter
        }

    }

    CoverActionList {
        id: coverAction

        CoverAction {
            iconSource: playerHandler.isPlaying ? "image://theme/icon-cover-pause" : "image://theme/icon-cover-play"
            onTriggered: {
                playerHandler.playpause()
            }
        }
    }
}
