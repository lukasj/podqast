import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.Portrait

    DockedPanel {
        id: parampanel
        width: parent.width
        height: parent.width
        dock: Dock.Top
        open: podqast.playparamdok
        z: 3

        Rectangle {
            anchors.fill: parent
            color: Theme.highlightDimmerColor
            opacity: 0.8
        }

        Slider {
            id: playrateSlider
            width: parent.width
            label: qsTr("Audio playrate")
            minimumValue: 0.85
            maximumValue: 2.0
            handleVisible: true
            valueText: "1:" + value
            stepSize: 0.15
            value: playerHandler.playrate
            onValueChanged: playerHandler.playrate = value
        }
        Row {
            anchors.top: playrateSlider.bottom
            width: parent.width
            height: Theme.itemSizeExtraLarge
            IconButton {
                id: sleepButton
                anchors.verticalCenter: parent.verticalCenter
                icon.source: "image://theme/icon-m-timer"
                highlighted: podqast.dosleep
                onClicked: {
                    // highlighted = !highlighted
                    podqast.dosleep = !podqast.dosleep
                }

                onPressAndHold: {
                    sleep = sleepTimerConf.value
                    sleepTimerSlider.value = sleep
                }
            }

            Slider {
                id: sleepTimerSlider
                highlighted: podqast.dosleep
                width: parent.width - sleepButton.width
                label: qsTr("Sleep timer") + (podqast.dosleep ? qsTr(" running...") : "")
                minimumValue: 0
                maximumValue: 60
                handleVisible: true
                valueText: value + "min"
                stepSize: 1
                value: sleep
                onValueChanged: {
                    sleep = value
                    console.log("sleep value changed: " + value)
                }
            }
        }


        IconButton {
            id: goup
            icon.source: "image://theme/icon-m-up"
            onClicked: podqast.playparamdok = false
            anchors.bottom: parent.bottom
            anchors.right: parent.right
        }
        Label {
            id: ainfo
            text: podqast.ainfolabel
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            width: parent.width - goup.width
            height: Theme.ItemSizeSmall
            wrapMode: Text.WordWrap
            font.pixelSize: Theme.fontSizeTiny
            color: Theme.highlightColor
        }
    }

    SilicaFlickable {
        id: playflick
        anchors.fill: parent

        Row {
            id: imagecol
            width: page.width
            height: page.width

            Image {
                id: podimage
                width: parent.width
                height: parent.width
                source: playerHandler.playicon === "" ? "../../images/podcast.png" : playerHandler.playicon
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        podqast.playparamdok = true
                    }
                }
                IconButton {
                    icon.source: "image://theme/icon-m-down"
                    visible: podqast.playparamdok === false
                    onClicked: podqast.playparamdok = true
                    anchors.top: parent.top
                    anchors.right: parent.right
                }
            }
        }

        Column {
            id: playcontrols
            anchors.bottom: parent.bottom
            width: page.width
            Row {
                id: chapterrow
                width: parent.width
                height: Theme.itemSizeExtraLarge
                IconButton {
                    id: lastchapter
                    enabled: playerHandler.chapters.length > 0
                    icon.source: "image://theme/icon-m-previous"
                    onClicked: {
                        if (playerHandler.chapters.length > 0) {
                            if (playerHandler.aktchapter !== 0) {
                                playerHandler.aktchapter -= 1
                                playerHandler.playpos = podqast.tomillisecs(playerHandler.chapters[playerHandler.aktchapter].start)
                                playerHandler.seek(playerHandler.playpos)
                                mediaplayer.seek(playerHandler.playpos)
                                chapterLabel.text = Number(playerHandler.aktchapter + 1) + ". " + playerHandler.chapters[playerHandler.aktchapter].title
                            }
                        }
                    }
                }
                MouseArea {
                    width: parent.width - lastchapter.width - nextchapter.width
                    height: parent.height
                    onClicked: {
                        if (playerHandler.chapters.length > 0) {
                            pageStack.push(Qt.resolvedUrl("Chapters.qml"), { episodeid: playerHandler.firstid })
                        }
                    }
                    Column {
                        id: infocolumn
                        width: parent.width

                        Label {
                            id: titlelabel
                            width: parent.width
                            text: playerHandler.firsttitle
                            font.pixelSize: Theme.fontSizeSmall
                            font.bold: true
                            wrapMode: Text.WordWrap
                        }

                        Label {
                            id: chapterLabel
                            enabled: playerHandler.chapters.length > 0
                            width: parent.width
                            text: playerHandler.chapters.length + qsTr(" chapters")
                            font.pixelSize: Theme.fontSizeExtraSmall
                            wrapMode: Text.WordWrap
                            Timer {
                                id: secs
                                interval: 1 * 1000
                                running: playerHandler.isPlaying
                                repeat: true
                                onTriggered: {
                                    if (playerHandler.chapters.length !== 0) {
                                        playerHandler.getaktchapter()
                                        chapterLabel.text = Number(playerHandler.aktchapter + 1) + ". " + playerHandler.chapters[playerHandler.aktchapter].title
                                    }
                                }
                            }
                        }
                    }
                }

                IconButton {
                    id: nextchapter
                    enabled: playerHandler.chapters.length > 0
                    icon.source: "image://theme/icon-m-next"
                    onClicked: {
                        if (playerHandler.chapters.length > 0) {
                            if (playerHandler.aktchapter !== playerHandler.chapters.length -1 ) {
                                playerHandler.nextchapter()
                                chapterLabel.text = Number(playerHandler.aktchapter + 1) + ". " + playerHandler.chapters[playerHandler.aktchapter].title
                            }
                        }
                    }
                }
            }

            Row {
                id: progressrow
                width: parent.width
                height: Theme.itemSizeLarge
                Slider {
                    id: playSlider
                    width: parent.width
                    leftMargin: 50
                    rightMargin: 50
                    height: parent.height
                    handleVisible: false
                    maximumValue: mediaplayer.duration
                    value: mediaplayer.position
                    valueText: to_pos_str(mediaplayer.position / 1000)
                    onPressedChanged: {
                        mediaplayer.seek(playSlider.value)
                    }
                }
            }
            Row {
                id: playerrow
                anchors.horizontalCenter: parent.horizontalCenter
                height: Theme.itemSizeLarge

                IconButton {
                    icon.source: "image://theme/icon-m-previous"
                    onClicked: {
                        podqast.fast_backward()
                    }
                }
                IconButton {
                    icon.source: "image://theme/icon-l-" + (playerHandler.isPlaying ? "pause" : "play")
                    onClicked: {
                        playerHandler.playpause()
                    }

                }
                IconButton {
                    icon.source: "image://theme/icon-m-next"

                    onClicked: {
                        podqast.fast_forward()
                    }
                }
            }
        }
    }
}
