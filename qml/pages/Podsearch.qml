import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4
import "../components"

Page {
    id: page
    property int margin: 16
    property var tagname

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent
        // contentHeight: page.height

        AppMenu { thispage: "Discover" }

        Row {
            id: discovertitle
            width: page.width
            Column {
                id: column

                width: page.width

                spacing: Theme.paddingLarge
                PageHeader {
                    title: tagname
                }
            }
        }

        Component.onCompleted: {
            gpodderhandler.getPodcasts(tagname)
        }

        Connections {
            target: gpodderhandler
            ignoreUnknownSignals: true

            onPodcastList: {
                podcastlistModel.clear();
                for (var i=0; i < pclist.length; i++) {
                    // console.log(JSON.stringify(pclist[i]));
                    podcastlistModel.append(pclist[i]);
                }
            }
        }

        SilicaListView {
            anchors.top: discovertitle.bottom

            width: parent.width;
            height: page.height - pdp.height - discovertitle.height
            model: ListModel {
                id: podcastlistModel
            }

            delegate: PodcastListItem {}
        }

        PlayDockedPanel { id: pdp }
    }
}
