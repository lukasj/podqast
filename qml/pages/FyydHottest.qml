import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4
import "../components"

Page {
    id: page
    property int margin: Theme.paddingMedium

    Component.onCompleted: {
        fyydhandler.getHotLangs(fyydHottestLangConf.value)
        fyydhandler.getHot(fyydHottestLangConf.value)
    }

    ListModel {
        id: langlistModel
    }

    Connections {
        target: fyydhandler
        onFyydHottest: {
            for (var i = 0; i < hottestlist.length; i++) {
                podslistModel.append(hottestlist[i]);
            }
        }
        onFyydHottestLangs: {
            langlistModel.clear()
            for (var i = 0; i < hottestlanglist.length; i++) {
                langlistModel.append({
                    "lang" : hottestlanglist[i]
                })
            }
        }
    }

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent
        id: theflick

        AppMenu { thispage: "Discover" }
        PrefAboutMenu {}

        // Tell SilicaFlickable the height of its content.
        contentHeight: page.height

        SilicaListView {
            id: hottestlist
            anchors.top: parent.top
            width: parent.width
            height: page.height - pdp.height

            header: Column {
                property alias langselect: langselect

                width: parent.width

                PageHeader {
                    title: qsTr("Hottest podcasts")
                }

                ComboBox {
                    id: langselect
                    width: page.width
                    label: qsTr("Language")

                    menu: ContextMenu {
                        Repeater {
                            model: langlistModel
                            MenuItem {
                                text:  model.lang
                                onClicked: {
                                    fyydHottestLangConf.value = model.lang
                                    podslistModel.clear()
                                    fyydhandler.getHot(model.lang)
                                }
                            }

                        }
                    }
                }
            }

            BusyIndicator {
                id: busyIndicator
                size: BusyIndicatorSize.Large
                anchors.centerIn: parent
                running: parent.count === 0
            }

            VerticalScrollDecorator { }

            model: ListModel {
                id: podslistModel
                function update() {
                    clear()
                }
            }

            delegate: PodcastListItem { }
        }

        PlayDockedPanel { id: pdp }
    }
}
