import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    SilicaFlickable {
        anchors.fill: parent

        VerticalScrollDecorator { }

        AppMenu { thispage: "Archive" }
        PrefAboutMenu { thelink: "https://gitlab.com/cy8aer/podqast/wikis/help-library" }

        // Tell SilicaFlickable the height of its content.
        contentHeight: page.height

        Component.onCompleted: {
            feedparserhandler.getPodcasts()
        }

        Column {
            property bool refreshing: false
            id: archivetitle

            width: page.width

            spacing: Theme.paddingLarge
            PageHeader {
                Column {
                    id: refreshcol
                    property real refreshprogress: 1.0
                    anchors.verticalCenter: parent.verticalCenter
                    IconButton {
                        id: refreshicon
                        enabled: refreshcol.refreshprogress == 1.0 && ! archivetitle.refreshing
                        icon.source: "image://theme/icon-m-refresh"
                        onClicked: {
                            // refresh()
                            if (wifiConnected || doMobileDownConf.value ) {
                                archivetitle.refreshing = true
                                feedparserhandler.refreshPodcasts()
                            }
                        }
                        ProgressCircle {
                            borderWidth: 2
                            width: parent.width * 0.8
                            height: parent.height * 0.8
                            visible: refreshcol.refreshprogress != 1.0
                            anchors.centerIn: refreshicon
                            progressValue: refreshcol.refreshprogress
                        }
                    }
                }

                title: qsTr("Library")
            }
        }

        Connections {
            target: feedparserhandler

            onNewPodcasts: {
                archivetitle.refreshing = false
            }
            onRefreshProgress: {
                console.log("Refresh progress" + progress)
                refreshcol.refreshprogress = progress
            }
        }

        SilicaListView {
            id: podcasts
            anchors.top: archivetitle.bottom
            width: parent.width
            height: page.height - pdp.height - archivetitle.height
            section.property: "bethead"
            section.delegate: sectionTile
            VerticalScrollDecorator {}

            Component {
                id: sectionTile
                Rectangle {
                    width: parent.width
                    height: Theme.itemSizeExtraSmall / 10
                    color: Theme.highlightDimmerColor
                }
            }

            Connections {
                target: feedparserhandler
                onPodcastsList: {
                    console.log("Data length: " + pcdata.length)
                    podcastsModel.clear()
                    podcastsModel.append({
                                         bethead: "00title",
                                         url: "",
                                         description: "",
                                         logo_url: "image://theme/icon-m-backup",
                                         title: qsTr("History"),
                                         topage: "History.qml"})
                    podcastsModel.append({
                                         bethead: "00title",
                                         url: "",
                                         description: "",
                                         logo_url: "image://theme/icon-m-favorite-selected",
                                         title: qsTr("Favorites"),
                                         topage: "Favorites.qml"})
                    if(allowExtConf.value) {
                        podcastsModel.append({
                                             bethead: "0ext",
                                             url: "",
                                             description: "",
                                             logo_url: "image://theme/icon-m-device-upload",
                                             title: qsTr("External Audio"),
                                             topage: "External.qml"})
                    }
                    for (var i = 0; i < pcdata.length; i++) {
                        console.log(pcdata[i])
                        pcdata[i]["bethead"] = "Podcast"
                        podcastsModel.append(pcdata[i]);
                    }
                }
                onGetPodcastsProgress: {
                    podcasts.loadedPodcasts = loadedpodcasts
                    podcasts.totalPodcasts = totalpodcasts
                }
            }

            //Basically a ViewPlaceholder with a ProgressCircle in the center
            property int loadedPodcasts
            property int totalPodcasts
            Item {
                anchors.centerIn: parent
                visible: podcasts.loadedPodcasts !== podcasts.totalPodcasts

                Label {
                    text: qsTr("Rendering")
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: spinningThing.top
                    font {
                        pixelSize: Theme.fontSizeExtraLarge
                        family: Theme.fontFamilyHeading
                    }
                    color: Theme.secondaryHighlightColor
                }

                ProgressCircle {
                    id: spinningThing
                    width: Theme.iconSizeExtraLarge
                    height: width
                    anchors.centerIn: parent
                    progressValue: podcasts.loadedPodcasts / podcasts.totalPodcasts
                    backgroundColor: Theme.secondaryHighlightColor
                }

                Label {
                    text: podcasts.loadedPodcasts + "/" + podcasts.totalPodcasts
                    anchors.centerIn: parent
                    color: Theme.secondaryHighlightColor
                }

                Label {
                    text: qsTr("Collecting Podcasts")
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: spinningThing.bottom
                    font {
                        pixelSize: Theme.fontSizeLarge
                        family: Theme.fontFamilyHeading
                    }
                    color: Theme.highlightColor
                    opacity: Theme.opacityLow
                }
            }

            model: ListModel {
                id: podcastsModel
            }
            delegate: PodcastItem { }
        }

        PlayDockedPanel { id: pdp }
    }
}
