import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page
    property var url
    property var link
    property var title
    property bool podpostslist: true

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    Component.onCompleted: {
        feedparserhandler.getEntries(url)
    }

    Connections {
        target: feedparserhandler
        ignoreUnknownSignals : true
        onCreatePodpostsList: {
            title = podtitle
            link = podlink
            podpostsModel.clear()
            for (var i = 0; i < pcdata.length; i++) {
                podpostsModel.append(pcdata[i]);
            }
        }
        onGetEntriesProgress: {
            archivepostlist.loadedEntries = loadedentries
            archivepostlist.totalEntries = totalentries
        }
    }

    // To enable PullDownMenu, place our content in a SilicaFlickable

    SilicaFlickable {
        anchors.fill: parent

        VerticalScrollDecorator { }

        AppMenu { thispage: "Archive" }
        PrefAboutMenu { thelink: "https://gitlab.com/cy8aer/podqast/wikis/help-podlist"}

        // Tell SilicaFlickable the height of its content.
        contentHeight: page.height

        Column {
            id: archivetitle

            width: page.width

            spacing: Theme.paddingLarge
            PageHeader {
                title: page.title
                Column {
                    id: placeholder
                    width: Theme.itemSizeExtraSmall / 2
                }

                Column {
                    id: prefscol
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: placeholder.right
                    IconButton {
                       id: prefsicon
                        icon.source: "image://theme/icon-m-developer-mode"
                        onClicked: {
                            pageStack.push(Qt.resolvedUrl("PodcastSettings.qml"),
                                       { podtitle: title, url: url })
                        }
                    }
                }

            }
        }

        SilicaListView {
            id: archivepostlist
            anchors.top: archivetitle.bottom
            width: parent.width
            height: page.height - pdp.height - archivetitle.height
            section.property: 'section'
            section.delegate: SectionHeader {
                text: section
                horizontalAlignment: Text.AlignRight
            }

            //Basically a ViewPlaceholder with a ProgressCircle in the center
            property int loadedEntries
            property int totalEntries
            Item {
                anchors.centerIn: parent
                visible: archivepostlist.loadedEntries != archivepostlist.totalEntries

                Label {
                    text: qsTr("Rendering")
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: spinningThing.top
                    font {
                        pixelSize: Theme.fontSizeExtraLarge
                        family: Theme.fontFamilyHeading
                    }
                    color: Theme.secondaryHighlightColor
                }

                ProgressCircle {
                    id: spinningThing
                    width: Theme.iconSizeExtraLarge
                    height: width
                    anchors.centerIn: parent
                    progressValue: archivepostlist.loadedEntries / archivepostlist.totalEntries
                    backgroundColor: Theme.secondaryHighlightColor
                }

                Label {
                    text: archivepostlist.loadedEntries + "/" + archivepostlist.totalEntries
                    anchors.centerIn: parent
                    color: Theme.secondaryHighlightColor
                }

                Label {
                    text: qsTr("Creating items")
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: spinningThing.bottom
                    font {
                        pixelSize: Theme.fontSizeLarge
                        family: Theme.fontFamilyHeading
                    }
                    color: Theme.highlightColor
                    opacity: Theme.opacityLow
                }
            }

            model: ListModel {
                id: podpostsModel
            }
            delegate: PodpostPostListItem { }
        }

        PlayDockedPanel { id: pdp }
    }
}
