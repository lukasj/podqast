import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent

        AppMenu { thispage: "Inbox" }
        PrefAboutMenu { thelink: "https://gitlab.com/cy8aer/podqast/wikis/help-inbox" }

        Component.onCompleted: {
            inboxhandler.getInboxEntries("home")
        }

        Connections {
            target: inboxhandler
            onCreateInboxList: {
                console.log("creating Inbox list")
                console.log(data.length)
                inboxPostModel.clear();
                for (var i=0; i < data.length; i++) {
                    console.log("element " + i)
                    inboxPostModel.append(data[i]);
                }
            }
            onGetInboxPosts: {
                inboxhandler.getInboxEntries(podqast.ifilter)
                inboxhandler.getInboxPodData()
            }
        }

        Connections {
            target: feedparserhandler
            onNewPodcasts: {
                inboxhandler.getInboxEntries(podqast.ifilter)
                inboxhandler.getInboxPodData()
            }
        }

        // Tell SilicaFlickable the height of its content.
        contentHeight: page.height

        Column {
            id: inboxtitle

            width: page.width

            spacing: Theme.paddingLarge
            PageHeader {
                title: qsTr("Inbox")
                Column {
                    id: allmovecol
                    anchors.verticalCenter: parent.verticalCenter
//                    anchors.left: placeholder.right
                    IconButton {
                       id: allmoveicon
                        icon.source: "image://theme/icon-m-backup"
                        onClicked: {
                            Remorse.popupAction(page, qsTr("Moving all posts to archive"), function() {
                                inboxhandler.moveAllArchive()
                            })
                        }
                    }
                }
            }
        }

//        PodSelectGrid {
//            id: podselectgrid
//            homeicon: "image://theme/icon-m-media-playlists"
//            anchors.top: inboxtitle.bottom
//            height: Theme.iconSizeLarge
//            z: 3
//            filter: podqast.ifilter
//            Component.onCompleted: {
//                // inboxhandler.getInboxPodData()
//            }
//            onFilterChanged: {
//                podqast.ifilter = filter
//                inboxPostModel.clear()
//                inboxhandler.getInboxPodData()
//                inboxhandler.getInboxEntries(podqast.ifilter)
//            }
//        }

        SilicaListView {
            id: inboxpostlist
            // anchors.top: podselectgrid.bottom
            anchors.top: inboxtitle.bottom
            width: parent.width
            height: page.height - pdp.height - inboxtitle.height // - podselectgrid.height
            section.property: 'section'
            section.delegate: SectionHeader {
                text: section
                horizontalAlignment: Text.AlignRight
            }
            ViewPlaceholder {
                enabled: inboxPostModel.count == 0
                text: qsTr("No new posts")
                hintText: qsTr("Pull down to Discover new podcasts, get posts from Library, or play the Playlist")
                verticalOffset: - inboxtitle.height // - podselectgrid.height
            }

            VerticalScrollDecorator { }

            model: ListModel {
                id: inboxPostModel
            }
            delegate: InboxPostListItem { }
        }

        PlayDockedPanel { id: pdp }
    }
}
