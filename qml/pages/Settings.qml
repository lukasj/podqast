import QtQuick 2.0
import Sailfish.Silica 1.0

Dialog {
    SilicaFlickable {
        anchors.fill: parent
        contentWidth: parent.width
        contentHeight: downloadConf.height + downloadConf.spacing + dialogheader.height + dialogheader.spacing

        property int refreshTime

        VerticalScrollDecorator { }

        DialogHeader {
            id: dialogheader
            // title: qsTr("Settings")
        }

        Column {
            anchors.top: dialogheader.bottom
            id: downloadConf
            width: parent.width
            spacing: Theme.paddingMedium

            Label {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }

                text: qsTr("Global Podcast Settings")
                font.pixelSize: Theme.fontSizeMedium
                color: Theme.highlightColor
            }

            ComboBox {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                id: moveTo
                width: parent.width
                label: qsTr("Move new post to")
                menu: ContextMenu {
                    MenuItem { text: qsTr("Inbox") }
                    MenuItem { text: qsTr("Top of Playlist") }
                    MenuItem { text: qsTr("Bottom of Playlist") }
                    MenuItem { text: qsTr("Archive") }
                }
            }
            Slider {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                id: autoLimit
                width: parent.width
                label: qsTr("Automatic post limit")
                minimumValue: 0
                maximumValue: 10
                handleVisible: true
                valueText: value == 0 ? "All" : value
                stepSize: 1
            }
            Slider {
                id: refreshTime
                function sectohhmm (secs) {
                    var date = new Date(null)
                    date.setSeconds(secs)
                    var timeString = date.toISOString().substr(11, 5)
                    if (timeString === "00:00") {
                        timeString = "24:00"
                    }

                    return timeString
                }

                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                width: parent.width
                label: qsTr("Refresh interval")
                minimumValue: 0.0
                maximumValue: 86400
                handleVisible: true
                valueText: refreshTime.value == 0 ? qsTr("off") : sectohhmm(refreshTime.value)
                stepSize: 900
            }

            Slider {
                id: globalPlayrate
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                width: parent.width
                label: qsTr("Audio playrate")
                minimumValue: 0.85
                maximumValue: 2.0
                handleVisible: true
                valueText: "1:" + value
                stepSize: 0.15
            }

            Slider {
                id: sleepTimer
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                width: parent.width
                label: qsTr("Sleep timer")
                minimumValue: 0
                maximumValue: 60
                handleVisible: true
                valueText: value + qsTr("min")
                stepSize: 1
            }

            Label {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                text: qsTr("External Audio")
                font.pixelSize: Theme.fontSizeMedium
                color: Theme.highlightColor
            }
            TextSwitch {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }

                id: allowExt
                text: qsTr("Allow external audio")
            }
            Label {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                text: qsTr("Will take data from\n") + StandardPaths.music + "/podqast/external"
                horizontalAlignment: Text.AlignRight
                font.pixelSize: Theme.fontSizeExtraSmall
            }
            ComboBox {
                enabled: allowExt.checked
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                id: extMoveTo
                width: parent.width
                label: qsTr("Move external audio to")
                menu: ContextMenu {
                    MenuItem { text: qsTr("Inbox") }
                    MenuItem { text: qsTr("Top of Playlist") }
                    MenuItem { text: qsTr("Bottom of Playlist") }
                    MenuItem { text: qsTr("Archive") }
                }
            }

            Label {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                text: qsTr("Download/Streaming")
                font.pixelSize: Theme.fontSizeMedium
                color: Theme.highlightColor
            }
//            anchors {
//                left: parent.left
//                right: parent.right
//                margins: Theme.paddingMedium
//            }
            TextSwitch {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }

                id: doDownload
                text: qsTr("Download Playlist Posts")
            }
            TextSwitch {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                id: doMobileDown
                text: qsTr("Download on Mobile")
            }
            TextSwitch {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                id: doFavDown
                text: qsTr("Keep Favorites downloaded")
            }
            Label {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                horizontalAlignment: Text.AlignRight
                text: qsTr("Will save data in\n") + StandardPaths.music + "/podqast/favorites"
                font.pixelSize: Theme.fontSizeExtraSmall
            }

//            Slider {
//                id: downLimit
//                width: parent.width
//                label: qsTr("Download limit")
//                minimumValue: 0
//                maximumValue: 2000
//                handleVisible: true
//                valueText: value ===0 ? "No limit" : value + "MB"
//                stepSize: 100
//            }
//            TextSwitch {
//                id: warnMobileStream
//                text: qsTr("Warn if streaming from Mobile")
//            }
//            TextSwitch {
//                id: saveOnSD
//                text: qsTr("Save data on SD card")
//            }
            Label {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                text: qsTr("System")
                font.pixelSize: Theme.fontSizeMedium
                color: Theme.highlightColor
            }
            TextSwitch {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                id: dataTrackable
                text: qsTr("Audio viewable in system")
            }
            Label {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                text: qsTr("Development")
                font.pixelSize: Theme.fontSizeMedium
                color: Theme.highlightColor
            }
            TextSwitch {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                id: experimentalFlag
                text: qsTr("Experimental features")
            }

//            Label {
//                text: qsTr("Privacy")
//                font.pixelSize: Theme.fontSizeMedium
//                color: Theme.highlightColor
//            }
//            TextSwitch {
//                id: useGpodder
//                text: qsTr("Use gpodder.net search")
//            }
        }
    }
    onOpened: {
        // useGpodder.checked = useGpodderConf.value
        dataTrackable.checked = dataTrackableConf.value
        // saveOnSD.checked = saveOnSDCardConf.value
        doDownload.checked = doDownloadConf.value
        doMobileDown.checked = doMobileDownConf.value
        doFavDown.checked = doFavDownConf.value
        moveTo.currentIndex = moveToConf.value
        allowExt.checked = allowExtConf.value
        extMoveTo.currentIndex = extMoveToConf.value
        autoLimit.value = autoLimitConf.value
        refreshTime.value = refreshTimeConf.value
        globalPlayrate.value = globalPlayrateConf.value
        experimentalFlag.checked = experimentalConf.value
        sleepTimer.value = sleepTimerConf.value
        // downLimit.value = downLimitConf.value
        // refreshTime = refreshTimeConf.value
    }
    onAccepted: {
        // useGpodderConf.value = useGpodder.checked
        dataTrackableConf.value = dataTrackable.checked

        feedparserhandler.nomedia(dataTrackableConf.value)

        // saveOnSDCardConf.value = saveOnSD.checked
        doDownloadConf.value = doDownload.checked
        doMobileDownConf.value = doMobileDown.checked
        doFavDownConf.value = doFavDown.checked
        moveToConf.value = moveTo.currentIndex
        allowExtConf.value = allowExt.checked
        extMoveToConf.value = extMoveTo.currentIndex
        autoLimitConf.value = autoLimit.value
        refreshTimeConf.value = refreshTime.value
        globalPlayrateConf.value = globalPlayrate.value
        experimentalConf.value = experimentalFlag.checked
        sleepTimerConf.value = sleepTimer.value
        // downLimitConf.value = downLimit.value
        // refreshTimeConf.value = refreshTime
    }
}
