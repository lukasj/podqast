import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4
import "../components"

Page {
    id: page
    allowedOrientations: Orientation.All
    property int margin: 16

    SilicaFlickable {
        id: mainflick
        anchors.fill: parent
        // contentHeight: page.height

        AppMenu { thispage: "Discover" }
        PrefAboutMenu {}

        Row {
            id: discovertitle
            width: page.width
            Column {
                id: column
                width: page.width
                spacing: Theme.paddingLarge

                PageHeader {
                    title: qsTr("Discover by URL")
                }
            }
        }
        Column {
            id: texturlcolumn
            anchors.top: discovertitle.bottom
            width: parent.width
            height: Theme.paddinglarge
            spacing: Theme.paddingMedium
            TextField {
                id: urltext
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                EnterKey.iconSource: "image://theme/icon-m-enter-accept"
                EnterKey.enabled: text.length > 0
                EnterKey.onClicked: pageStack.push(Qt.resolvedUrl("Podcast.qml"), { url: urltext.text })

                width: parent.width
                focus: true
                placeholderText: qsTr("Enter podcasts' feed url")
                inputMethodHints: Qt.ImhUrlCharactersOnly
                // regExp from https://stackoverflow.com/questions/13957151/regexpvalidator-does-not-validate-a-url-pattern-correctly
                // validator: RegExpValidator { regExp: /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/ }
            }
        }
        Column {
            id: hintcolumn
            height: Theme.itemSizeExtraSmall
            anchors.top: texturlcolumn.bottom
            spacing: Theme.paddingMedium
            Label {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                text: qsTr("Please enter feed urls here - not web page urls")
                font.pixelSize: Theme.fontSizeExtraSmall
            }
        }


        PlayDockedPanel { }
    }
}
