import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4
import "../components"

Page {
    id: page
    property int margin: Theme.paddingMedium

    Component.onCompleted: {
        fyydhandler.getCategories()
    }

    Connections {
        target: fyydhandler

        onFyydCategories: {
            for (var i = 0; i < categories.length; i++) {
                categorieslistModel.append(categories[i]);
            }
        }
    }

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent
        id: theflick

        AppMenu { thispage: "Discover" }
        PrefAboutMenu {}

        // Tell SilicaFlickable the height of its content.
        contentHeight: page.height

        SilicaListView {
            id: categorieslist
            anchors.top: parent.top
            width: parent.width
            height: page.height - pdp.height

            header: PageHeader {
                    title: qsTr("Categories")
            }

            model: ListModel {
                id: categorieslistModel
            }

            BusyIndicator {
                id: busyIndicator
                size: BusyIndicatorSize.Large
                anchors.centerIn: parent
                running: parent.count === 0
            }

            VerticalScrollDecorator { }

            delegate: ListItem {
                height: Theme.itemSizeExtraSmall
                onClicked: pageStack.push(
                               Qt.resolvedUrl("FyydCategory.qml"),
                               { category_name: name, category_id: id}
                               )
                Label {
                    anchors.left: parent.left
                    anchors.leftMargin: margin
                    anchors.verticalCenter: parent.verticalCenter
                    text: name
                }
                IconButton {
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    icon.source: "image://theme/icon-m-right"
                    onClicked: pageStack.push(
                                   Qt.resolvedUrl("FyydCategory.qml"),
                                   { category_name: name, category_id: id}
                                   )
                }
            }
        }

        PlayDockedPanel { id: pdp }
    }
}
