import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4
import "../components"

Page {
    id: page
    property int margin: Theme.paddingMedium

    Component.onCompleted: {
        searcherlist.headerItem.searchField.forceActiveFocus()
    }

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent
        id: theflick

        AppMenu { thispage: "Discover" }
        PrefAboutMenu {}

        // Tell SilicaFlickable the height of its content.
        contentHeight: page.height
        Row {
            id: discovertitle
            width: page.width
            Column {
                id: column

                width: page.width

                spacing: Theme.paddingLarge
                PageHeader {
                    title: qsTr("Discover by Search")
                }
            }
        }

        Connections {
            target: gpodderhandler
            onPodSearch: {
                podslistModel.clear();
                for (var i=0; i < podcasts.length; i++) {
                    if (i < 6) {
                        podslistModel.append(podcasts[i]);
                    }
                }
            }
        }

        SilicaListView {
            id: searcherlist
            anchors.top: discovertitle.bottom
            width: parent.width
            height: 1000

            header: Item {
                property alias searchField: searchField

                width: parent.width
                height: Theme.itemSizeLarge

                SearchField {
                    id: searchField
                    placeholderText: qsTr("Search")
                    anchors.left: parent.left
                    width: parent.width
                    onTextChanged: {
                        if (searchField.text.length > 3) {
                            gpodderhandler.doSearch(searchField.text.trim())
                        }
                    }
                }
            }

            model: ListModel {
                id: podslistModel
            }

            delegate: PodcastListItem { }
        }

        PlayDockedPanel { }
    }
}
