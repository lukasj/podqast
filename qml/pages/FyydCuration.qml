import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4
import "../components"

Page {
    id: page
    property int margin: Theme.paddingMedium
    property var category_name
    property int category_id
    property int curation_page: 0
    property bool busy: true

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent

        AppMenu { thispage: "Discover" }

        Component.onCompleted: {
            fyydhandler.getCurations(category_id,curation_page,10)
        }

        Connections {
            target: fyydhandler
            ignoreUnknownSignals: true

            onPodcastList: {
                for (var i=0; i < pclist.length; i++) {
                    podcastlistModel.append(pclist[i]);
                }
                busy = false
            }
        }

        SilicaListView {
            anchors.top: parent.top

            width: parent.width;
            height: page.height - pdp.height

            header: PageHeader {
                title: category_name
            }

            model: ListModel {
                id: podcastlistModel
            }

            delegate: PodcastListItem {}

            onContentYChanged: {
                if (!page.busy &&
                        indexAt(contentX, contentY + height) > podcastlistModel.rowCount() - 2) {
                    page.busy = true
                    curation_page = curation_page + 1
                    fyydhandler.getCurations(category_id,curation_page,10)
                }
            }
        }

        PlayDockedPanel { id: pdp }
    }
}
