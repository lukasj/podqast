import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4
import "../components"

Page {
    id: page
    property int margin: Theme.paddingMedium

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent
        id: theflick

        AppMenu { thispage: "Discover" }
        PrefAboutMenu {}

        // Tell SilicaFlickable the height of its content.
        contentHeight: page.height

        SilicaListView {
            id: searcherlist
            anchors.top: parent.top
            width: parent.width
            height: page.height - pdp.height



            header: Column {
                width: parent.width

                PageHeader {
                    title: qsTr("Discover on fyyd.de")
                }
            }

            model: ListModel {
                id: menumodel
                ListElement {
                    title: qsTr("Keyword Search")
                    nextpage: "FyydSearch.qml"
                }
                ListElement {
                    title: qsTr("Hottest Podcasts")
                    nextpage: "FyydHottest.qml"
                }
                ListElement {
                    title: qsTr("Browse Categories")
                    nextpage: "FyydCategories.qml"
                }
                ListElement {
                    title: qsTr("Browse Curations")
                    nextpage: "FyydCurations.qml"
                }
            }

            delegate: ListItem {
                onClicked: pageStack.push(Qt.resolvedUrl(nextpage))
                Label {
                    anchors.left: parent.left
                    anchors.leftMargin: margin
                    anchors.verticalCenter: parent.verticalCenter
                    text: title
                }
                IconButton {
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    icon.source: "image://theme/icon-m-right"
                    onClicked: pageStack.push(Qt.resolvedUrl(nextpage))
                }
            }

            VerticalScrollDecorator { }
        }

        PlayDockedPanel { id: pdp }
    }
}
