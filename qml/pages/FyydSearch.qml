import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4
import "../components"

Page {
    id: page
    property int margin: Theme.paddingMedium
    property int search_page: 0
    property bool busy: true

    Component.onCompleted: {
        searcherlist.headerItem.searchField.forceActiveFocus()
    }

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent
        id: theflick

        AppMenu { thispage: "Discover" }
        PrefAboutMenu {}

        // Tell SilicaFlickable the height of its content.
        contentHeight: page.height

        Connections {
            target: fyydhandler
            onPodSearch: {
                if (podcasts.length === 0 & podslistModel.rowCount() === 0) {
                    viewPlaceholder.text = qsTr("No results")
                    viewPlaceholder.hintText = qsTr("Try searching for something else")
                }
                for (var i=0; i < podcasts.length; i++) {
                    podslistModel.append(podcasts[i]);
                }
                busy = false
            }
        }

        SilicaListView {
            id: searcherlist
            anchors.top: parent.top
            width: parent.width
            height: page.height - pdp.height

            header: Column {
                property alias searchField: searchField

                width: parent.width

                PageHeader {
                    title: qsTr("Discover on fyyd.de")
                }

                SearchField {
                    id: searchField
                    placeholderText: qsTr("Search")
                    anchors.left: parent.left
                    width: parent.width

                    EnterKey.enabled: text.length > 0
                    EnterKey.iconSource: "image://theme/icon-m-enter-accept"
                    EnterKey.onClicked: {
                        busy = true
                        search_page = 0
                        podslistModel.clear()
                        fyydhandler.doSearch(searchField.text,search_page,10)
                        viewPlaceholder.text = ""
                        viewPlaceholder.hintText = ""
                    }
                }
            }

            onContentYChanged: {
                if (!page.busy &&
                        indexAt(contentX, contentY + height) > podslistModel.rowCount() - 2) {
                    page.busy = true
                    search_page = search_page + 1
                    fyydhandler.doSearch(searcherlist.headerItem.searchField.text,search_page,10)
                }
            }

            ViewPlaceholder {
                id: viewPlaceholder
                enabled: text
                text: qsTr("Here be podcasts")
                hintText: qsTr("Enter search term above")
            }

            BusyIndicator {
                id: busyIndicator
                size: BusyIndicatorSize.Large
                anchors.centerIn: parent
                running: parent.count === 0 && !viewPlaceholder.enabled
            }

            VerticalScrollDecorator { }

            model: ListModel {
                id: podslistModel
            }

            delegate: PodcastListItem { }
        }

        PlayDockedPanel { id: pdp }
    }
}
