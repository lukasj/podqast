import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Thumbnailer 1.0

ListItem {
    width: ListView.view.width
    contentHeight: Theme.itemSizeExtraLarge * 1.3
    // onClicked: openMenu()
    onClicked: pageStack.push(Qt.resolvedUrl("../pages/PostDescription.qml"), { title: title, detail: detail,
                                  length: length, date: fdate, duration: duration, href: link })
    onPressAndHold: console.log("press and hold")
    clip: true

    menu: IconContextMenu {
        id: contextMenu
        IconMenuItem {
            text: 'Play'
            icon.source: 'image://theme/icon-m-play'
            onClicked: {
                inboxhandler.moveQueueTop(id);
                closeMenu();
                inboxPostModel.remove(index)
                playerHandler.playicon=logo_url;
                playerHandler.playtext=title;
            }
        }
        IconMenuItem {
            text: 'QueueTop'
            icon.source: 'image://theme/icon-m-up'
            onClicked: {
                inboxhandler.moveQueueNext(id);
                closeMenu()
                inboxPostModel.remove(index)
            }
        }
        IconMenuItem {
            text: 'QueueBottom'
            icon.source: 'image://theme/icon-m-down'
            onClicked: {
                inboxhandler.moveQueueBottom(id);
                closeMenu()
                inboxPostModel.remove(index)
            }
        }
        IconMenuItem {
            text: 'Archive'
            icon.source: 'image://theme/icon-m-backup'
            onClicked: {
                inboxhandler.moveArchive(id);
                closeMenu()
                inboxPostModel.remove(index)
            }
        }
        IconMenuItem {
            id: chapterselection
            text: qsTr("Select chapters")
            icon.source: 'image://theme/icon-m-menu'
            opacity: (haschapters) ? 1 : 0.3
            enabled: (haschapters)
            onClicked: {
                pageStack.push(Qt.resolvedUrl("../pages/Chapters.qml"), { episodeid: id })
            }
        }
    }

    Rectangle {
        id: charrect
        anchors.fill: listicon
        visible: logo_url == ""
        color: Theme.rgba(Theme.highlightColor, 0.5)

        clip: true

        Label {
            anchors.centerIn: parent

            font.pixelSize: parent.height * 0.8
            text: title[0]
            color: Theme.highlightColor
        }
    }
    Thumbnail {
        id: listicon
        property int percentage: 0
        anchors.leftMargin: Theme.paddingMedium

        MouseArea {
            anchors.fill: parent
            onClicked: {
                 pageStack.push(Qt.resolvedUrl("../pages/PodpostList.qml"), { url: url })
            }
        }
        anchors.left: parent.left
        width: Theme.iconSizeLarge
        height: Theme.iconSizeLarge
        sourceSize.width: Theme.iconSizeLarge
        sourceSize.height: Theme.iconSizeLarge
        anchors.verticalCenter: parent.verticalCenter
        source: logo_url == "" ? "../../images/podcast.png" : logo_url
        Rectangle {
            id: dlstatus
            visible: listicon.percentage > 0 && listicon.percentage < 100
            anchors.right: parent.right
            height: parent.height
            width: (100 - listicon.percentage) * parent.width / 100
            color: "black"
            opacity: 0.7
            z: 0
        }
        Image {
            source: loaded ? "../../images/audio-l.png" : "../../images/audio.png"
            visible: isaudio
            z: 1
            width: parent.width / 4
            height: parent.height / 4
            anchors.bottom: parent.bottom
            anchors.right: parent.right
        }
        Connections {
            target: queuehandler
            onDownloading: {
                if (dlid == id) {
                    listicon.percentage = percent
                }
            }
        }
    }


    Column {
        anchors.left: listicon.right
        anchors.right: listarrow.left
        anchors.margins: Theme.paddingMedium
        height: parent.height
        clip: true

        Label {
            id: queueTitlelabel
            width: parent.width
            text: title
            font.pixelSize: Theme.fontSizeExtraSmall
            font.bold: true
            wrapMode: Text.WordWrap
            padding: 5
        }
        Label {
            id: queueTimelabel
            width: parent.width
            font.pixelSize: Theme.fontSizeTiny
            wrapMode: Text.WordWrap
            textFormat: Text.StyledText
            text: timestring == "" ? description : timestring + " · " + description
            padding: 5
        }
    }

    IconButton {
        id: listarrow
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        icon.source: "image://theme/icon-m-right"
        onClicked: {
            pageStack.push(Qt.resolvedUrl("../pages/PostDescription.qml"), { title: title, detail: detail,
                               length: length, date: fdate, duration: duration, href: link })
        }
    }
}
