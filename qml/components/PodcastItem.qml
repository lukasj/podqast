import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Thumbnailer 1.0

ListItem {
    id: podcastItem
    width: ListView.view.width
    contentHeight: topage ? Theme.itemSizeMedium : Theme.itemSizeLarge
    onClicked:  if(topage) {
                    pageStack.push(Qt.resolvedUrl(("../pages/" + topage)))
                } else {
                    pageStack.push(Qt.resolvedUrl("../pages/PodpostList.qml"), { url: url, title: title })
                }

    menu: Component {
        IconContextMenu {
            visible: !topage
            enabled: !topage
            id: podcastContextMenu
            IconMenuItem {
                text: qsTr('Refresh')
                icon.source: 'image://theme/icon-m-refresh'
                onClicked: {
                    closeMenu()
                    feedparserhandler.refreshPodcast(url)
                }
            }
            IconMenuItem {
                text: qsTr('Settings')
                icon.source: 'image://theme/icon-m-developer-mode'
                onClicked: {
                    closeMenu()
                    pageStack.push(Qt.resolvedUrl("../pages/PodcastSettings.qml"), { podtitle: title, url: url })
                }
            }
            IconMenuItem {
                text: qsTr('Delete')
                icon.source: 'image://theme/icon-m-delete'
                onClicked: {
                    var fph = feedparserhandler
                    var theurl = url
                    var pcm = podcastsModel
                    var idx = index
                    closeMenu()
                    Remorse.itemAction(podcastItem, qsTr("Deleted Podcast"), function() {
                        fph.deletePodcast(theurl)
                        pcm.remove(idx)
                    })
                }
            }
        }
    }
    Image {
        id: listicon

        anchors.left: parent.left
        width: topage? Theme.iconSizeMedium : Theme.iconSizeLarge
        height: topage? Theme.iconSizeMedium : Theme.iconSizeLarge
        sourceSize.width: topage? Theme.iconSizeMedium : Theme.iconSizeLarge
        sourceSize.height: topage? Theme.iconSizeMedium : Theme.iconSizeLarge
        anchors.margins: Theme.paddingMedium
        anchors.verticalCenter: parent.verticalCenter
        source: if (topage) {
                    logo_url
                } else {
                    logo_url == "" ? "../../images/podcast.png" : "image://nemoThumbnail/" + logo_url
                }
    }

    Label {
        id: titlelabel
        anchors.margins: Theme.paddingMedium
        anchors.left: listicon.right
        anchors.right: listarrow.left
        height: parent.height
        text: title
        font.pixelSize: Theme.fontSizeSmall
        font.bold: true
        wrapMode: Text.WordWrap
    }
    IconButton {
        id: listarrow
        height: parent.height
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        icon.source: "image://theme/icon-m-right"
        onClicked: {
            if(topage) {
                pageStack.push(Qt.resolvedUrl(("../pages/" + topage)))
            } else {
                pageStack.push(Qt.resolvedUrl("../pages/PodpostList.qml"), { url: url, title: title })
            }
        }
    }
}
