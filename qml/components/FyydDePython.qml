import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4

Python {
    id: fyydhandler

    signal podSearch(var podcasts)
    signal fyydHottest(var hottestlist)
    signal fyydHottestLangs(var hottestlanglist)
    signal fyydCategories(var categories)
    signal podcastList(var pclist)

    Component.onCompleted: {
        setHandler("podsearch", podSearch)
        setHandler("fyydhottest", fyydHottest)
        setHandler("fyydhottestlangs", fyydHottestLangs)
        setHandler("fyydcategories", fyydCategories)
        setHandler("podcastlist", podcastList)

        addImportPath(Qt.resolvedUrl('.'));
        importModule('FyydDe', function () {
            console.log('FyydDe is now imported')
        })
    }

    function doSearch(query, page, count) {
        call("FyydDe.fyydde.searchpods", [query, page, count], function() {});
    }

    function getHot(language) {
        call("FyydDe.fyydde.gethottest", [language], function() {});
    }

    function getHotLangs(language) {
        call("FyydDe.fyydde.gethottestlangs", [language], function() {});
    }

    function getCategories() {
        call("FyydDe.fyydde.getcategories", function() {});
    }

    function getCategory(category_id, page, count) {
        call("FyydDe.fyydde.getcategory", [category_id, page, count], function() {});
    }

    function getCurations(category_id, page, count) {
        call("FyydDe.fyydde.getcurations", [category_id, page, count], function() {});
    }
}
