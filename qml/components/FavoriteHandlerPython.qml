import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4

Python {
    id: favoritehandler

    signal favoriteToggled(var podpost, bool favstate)
    signal favoriteList(var favorites)

    Component.onCompleted: {
        setHandler("favoriteToggled", favoriteToggled)
        setHandler("favoriteList", favoriteList)

        addImportPath(Qt.resolvedUrl('.'));
        importModule('FavoriteHandler', function () {
            console.log('FavoriteHandler is now imported')
        })
    }

    function toggleFavorite(podpost) {
        // call("FavoriteHandler.favoritehandler.togglefavorite", [podpost, doFavDownConf.value], function() {});
        call("FavoriteHandler.toggle_favorite", [podpost, doFavDownConf.value], function() {});
    }
    function getFavorites(podurl) {
        if (podurl === "home") {
            call("FavoriteHandler.favoritehandler.getfavorites", function() {});
            // call("FavoriteHandler.get_favorites", function() {});
        } else {
            call("FavoriteHandler.favoritehandler.getfavorites", [podurl], function() {});
            // call("FavoriteHandler.get_favorites", [podurl], function() {});
        }

    }
    function getFavPodData() {
        call("FavoriteHandler.favoritehandler.getfavpoddata", function() {});
        // call("FavoriteHandler.get_fav_pod_data", function() {});
    }
}
