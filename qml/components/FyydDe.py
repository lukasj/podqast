#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pyotherside
import threading
import sys
import urllib.request
import urllib.parse
import json

sys.path.append("/usr/share/harbour-podqast/python")


def search_pods(query, page=0, count=10):
    """
    Search for podcast list
    query: the search query
    """
    pclist = []

    params = urllib.parse.urlencode(
        {"term": query, "count": count, "page": page}
        )
    url = "https://api.fyyd.de/0.2/search/podcast?%s" % params
    response = urllib.request.urlopen(url)

    podcasts = json.load(response)['data']
    for podcast in podcasts:
        title = podcast['title']
        if len(title) > 50:
            title = title[:50] + "..."
        description = podcast['description']
        if len(description) > 160:
            description = description[:160] + "..."
        if not podcast['imgURL']:
            logo_url = ""
        else:
            logo_url = podcast['thumbImageURL']

        pclist.append(
            {
                "url": podcast['xmlURL'],
                "title": title,
                "titlefull": podcast['title'],
                "description": description,
                "website": podcast['htmlURL'],
                "logo_url": logo_url,
            }
        )

    pyotherside.send("podsearch", pclist)


def get_hottest(language='en'):
    """
    Get list of hottest podcasts from fyyd.de
    language: two letters
    """
    pclist = []

    params = urllib.parse.urlencode({"language": language, "count": 20})
    url = "https://api.fyyd.de/0.2/feature/podcast/hot?%s" % params
    response = urllib.request.urlopen(url)

    podcasts = json.load(response)['data']
    for podcast in podcasts:
        title = podcast['title']
        if len(title) > 50:
            title = title[:50] + "..."
        description = podcast['description']
        if len(description) > 160:
            description = description[:160] + "..."
        if not podcast['imgURL']:
            logo_url = ""
        else:
            logo_url = podcast['thumbImageURL']

        pclist.append(
            {
                "url": podcast['xmlURL'],
                "title": title,
                "titlefull": podcast['title'],
                "description": description,
                "website": podcast['htmlURL'],
                "logo_url": logo_url,
            }
        )

    pyotherside.send("fyydhottest", pclist)


def get_hottest_langs(last_language='en'):
    """
    Get list of hottest podcasts from fyyd.de
    last_language: language to be returned on top of the list
    """
    langlist = []

    url = "https://api.fyyd.de/0.2/feature/podcast/hot/languages"
    response = urllib.request.urlopen(url)

    langlist = json.load(response)['data']
    langlist.sort()

    # move last language to first position in list
    langlist.sort(key=last_language.__ne__)

    pyotherside.send("fyydhottestlangs", langlist)


def get_category(category_id, page=0, count=50):
    """
    Get list of podcasts in a category
    """
    pclist = []

    params = urllib.parse.urlencode(
        {"category_id": category_id, "count": count, "page": page}
        )
    url = "https://api.fyyd.de/0.2/category?%s" % params
    response = urllib.request.urlopen(url)

    podcasts = json.load(response)['data']['podcasts']

    for podcast in podcasts:
        title = podcast['title']
        if len(title) > 50:
            title = title[:50] + "..."
        description = podcast['description']
        if len(description) > 160:
            description = description[:160] + "..."
        if not podcast['imgURL']:
            logo_url = ""
        else:
            logo_url = podcast['thumbImageURL']

        pclist.append(
            {
                "url": podcast['xmlURL'],
                "title": title,
                "titlefull": podcast['title'],
                "description": description,
                "website": podcast['htmlURL'],
                "logo_url": logo_url,
            }
        )

    pyotherside.send("podcastlist", pclist)


def get_categories():
    """
    Get list of hottest podcasts from fyyd.de
    last_language: language to be returned on top of the list
    """
    catlist = []

    url = "https://api.fyyd.de/0.2/categories"
    response = urllib.request.urlopen(url)

    categories = json.load(response)['data']

    for cat in categories:
        name = cat['name']
        cat_id = cat['id']
        name_de = cat['name_de']

        catlist.append(
            {
                "name": name,
                "name_de": name_de,
                "id": cat_id,
            }
        )

        for subcat in cat['subcategories']:
            name = '    ' + subcat['name']  # layman's way of indenting subcategories
            name_de = '    ' + subcat['name_de']  # layman's way of indenting subcategories
            cat_id = subcat['id']
            catlist.append(
                {
                    "name": name,
                    "name_de": name_de,
                    "id": cat_id,
                }
            )

    pyotherside.send("fyydcategories", catlist)


def get_curations(category_id, page=0, count=50):
    """
    Get list of curations inside a category
    """
    pclist = []

    params = urllib.parse.urlencode(
        {"category_id": category_id, "count": count, "page": page}
        )
    url = "https://api.fyyd.de/0.2/category/curation?%s" % params
    response = urllib.request.urlopen(url)

    curations = json.load(response)['data']['curations']
    for curation in curations:
        title = curation['title']
        if len(title) > 50:
            title = title[:50] + "..."
        description = curation['description']
        if len(description) > 160:
            description = description[:160] + "..."
        if not curation['layoutImageURL']:
            logo_url = ""
        else:
            logo_url = curation['thumbImageURL']

        pclist.append(
            {
                "url": curation['xmlURL'],
                "title": title,
                "titlefull": curation['title'],
                "description": description,
                "website": curation['url'],
                "logo_url": logo_url,
            }
        )

    pyotherside.send("podcastlist", pclist)


class FyydDe:
    def __init__(self):
        self.bgthread = threading.Thread()
        self.bgthread.start()

    def searchpods(self, query, page=0, count=10):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(
                target=search_pods, args=[query, page, count]
                )
        self.bgthread.start()

    def gethottest(self, language='en'):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_hottest, args=[language])
        self.bgthread.start()

    def gethottestlangs(self, last_language='en'):
        langlist = get_hottest_langs(last_language)
        return langlist

    def getcategories(self):
        categories = get_categories()
        return categories

    def getcategory(self, category_id, page=0, count=20):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(
                target=get_category, args=[category_id, page, count]
                )
        self.bgthread.start()

    def getcurations(self, category_id, page=0, count=20):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(
                target=get_curations, args=[category_id, page, count]
                )
        self.bgthread.start()


fyydde = FyydDe()
