import QtQuick 2.0
import io.thp.pyotherside 1.4

Python {
    id: playerHandler

    property bool isPlaying: false
    property string playicon: "../../images/podcast.png"
    property string playtext: ""
    property string firstid: ""
    property string firsttitle: ""
    property var chapters
    property int aktchapter
    property double playpos: 0
    property double playrate: 1.0
    property double seekPos

    signal playing(string audio_url, int position)
    signal pausing()
    signal stopping()
    signal downloading(int percent)
    signal audioNotExist()

    Component.onCompleted: {
        setHandler('playing', playing)
        setHandler('pausing', pausing)
        setHandler('downloading', downloading)
        setHandler('stopping', stopping)
        setHandler('audioNotExist', audioNotExist)

        addImportPath(Qt.resolvedUrl('components'));
        importModule('QueueHandler', function () {
            console.log('QueueHandler is now imported')
        })
    }

    onPlayrateChanged: {
        seekPos = mediaplayer.position
        mediaplayer.playbackRate = playrate
        mediaplayer.seek(seekPos)
    }

    onPlaying: {
        console.log("Audio_url: " + audio_url)
        console.log("Seekable: " + mediaplayer.seekable)
        if (audio_url != "") {
            playrate = globalPlayrateConf.value
            mediaplayer.source = audio_url
            mediaplayer.seek(position - 15 * 1000)
            mediaplayer.play()
            mediaplayer.seek(position)
            seekPos = position
        } else {
            mediaplayer.play()
        }

        mediaplayer.playbackRate = playrate

        console.log("Duration: ", mediaplayer.duration)
        console.log("Position: ", mediaplayer.position)
        isPlaying=true
        queuehandler.queueHrTime()
        externalhandler.getAudioData(audio_url)
    }
    onPausing: {
        mediaplayer.pause()
        isPlaying=false
        queuehandler.queueHrTime()
    }
    onStopping: {
        // mediaplayer.stop()
        mediaplayer.pause()
        isPlaying=false
        queuehandler.queueHrTime()
    }

    onDownloading: {

    }

    function getaktchapter() {
        if (chapters.length === 0) {
            return
        }
        var thepos = mediaplayer.position
        for (var i = 0; i < chapters.length; i++) {
            var startpos = tomillisecs(chapters[i].start)
           if (startpos > thepos) {
                aktchapter = i - 1
                return
            }
        }
        aktchapter = chapters.length - 1
    }

    function nextchapter() {
        console.log("started nextchapter()")
        if (aktchapter !== chapters.length -1 ) {
            aktchapter += 1
            playpos = podqast.tomillisecs(chapters[aktchapter].start)
            seek(playpos)
            mediaplayer.seek(playpos)
        }
        else {
            //go to end of track if there is no next chapter
            playpos = mediaplayer.duration
            seek(playpos)
            mediaplayer.seek(playpos)
        }
    }

    function nextselectedchapter() {
        //skip to the next selected chapter
        for (var i = aktchapter; i < chapters.length; i++) {
            console.log(i,chapters.length)
            if (chapters[i].selected === true) {
                playpos = podqast.tomillisecs(chapters[i].start)
                seek(playpos)
                mediaplayer.seek(playpos)
                return
            }
        }
        //go to end of track if there is no next chapter
        console.log("no more selected chapters. skipping to end of track.")
        playpos = mediaplayer.duration
        seek(playpos)
        mediaplayer.seek(playpos)
    }

    function play() {
        // call("QueueHandler.queuehandler.queueplay", function() {});
        call("QueueHandler.queue_play", function() {});
    }

    function pause() {
        call("QueueHandler.queuehandler.queuepause", [mediaplayer.position], function() {});
        // call("QueueHandler.queue_pause", [mediaplayer.position], function() {});
    }

    function playpause() {
        isPlaying = ! isPlaying;
        if (isPlaying) {
            play()
        } else {
            pause()
        }
    }

    function stop() {
        console.log("mediaplayer.position" + mediaplayer.position)
        call("QueueHandler.queuehandler.queuestop", [mediaplayer.position], function() {});
        // call("QueueHandler.queue_stop", [mediaplayer.position], function() {});
    }

    function seek(position) {
        call("QueueHandler.queuehandler.queueseek", [position], function() {});
        // call("QueueHandler.queue_seek", [position], function() {});
    }

    function setDuration() {
        console.log("mediaplayer.duration" + mediaplayer.duration / 1000)
        call("QueueHandler.queuehandler.setduration", [mediaplayer.duration / 1000], function() {})
        // call("QueueHandler.set_duration", [mediaplayer.duration / 1000], function() {})
    }
}
