#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pyotherside
import threading
import sys

sys.path.append("/usr/share/harbour-podqast/python")

from podcast.queue import QueueFactory
from podcast.factory import Factory
from podcast.archive import ArchiveFactory


def get_queue_posts(moved=""):
    """
    Return a list of all queue posts
    """

    entries = []
    queue = QueueFactory().get_queue()

    for post in queue.get_podposts():
        entry = Factory().get_podpost(post)
        edata = entry.get_data()
        edata["moved"] = edata["id"] == moved
        entries.append(edata)
        if len(entries) == 1:
            if edata["haschapters"]:
                chapters = entry.get_chapters()
            else:
                chapters = []
            pyotherside.send("setFirst", entries[0], chapters)
    pyotherside.send("createList", entries, moved)


def get_first_entry():
    """
    Return the data of the first entry
    """

    queue = QueueFactory().get_queue()
    if len(queue.podposts) < 1:
        return None
    pyotherside.send(
        "firstentry", Factory().get_podpost(queue.podposts[0]).get_data()
    )


def queue_insert_top(id, doplay=False):
    """
    Insert Podpost element to top of queue
    """

    queue = QueueFactory().get_queue()
    if queue.insert_top(id) == 1:
        pyotherside.send("stopped in insert_top")
        get_queue_posts()


def queue_insert_next(id, doplay=False):
    """
    Insert Podpost at next Entry in Queue
    """

    # pyotherside.send(doplay)
    queue = QueueFactory().get_queue()
    queue.insert_next(id)


def queue_insert_bottom(id, doplay=False):
    """
    Insert Podpost at last position in queue
    """

    queue = QueueFactory().get_queue()
    queue.insert_bottom(id)


def queue_move_up(id):
    """
    move element up
    """

    QueueFactory().get_queue().move_up(id)


def queue_move_down(id):
    """
    move element down
    """

    QueueFactory().get_queue().move_down(id)


def queue_play():
    """
    Get the play information from queue
    """

    queue = QueueFactory().get_queue()
    data = queue.play()

    if not data["url"]:
        pyotherside.send("audioNotExist")
    else:
        pyotherside.send("playing", data["url"], data["position"])


def queue_pause(position):
    """
    Set Pause
    """

    queue = QueueFactory().get_queue()

    queue.pause(position)

    pyotherside.send("pausing")


def queue_stop(position):
    """
    Stop and save position
    """

    queue = QueueFactory().get_queue()
    queue.stop(position)
    queue.save()
    pyotherside.send("stopping")


def queue_seek(position):
    """
    Set queue position
    """

    queue = QueueFactory().get_queue()
    queue.position(position)
    queue.save()
    pyotherside.send("positioning")


def queue_to_archive(id):
    """
    Move an item to archive
    """

    queue = QueueFactory().get_queue()
    archive = ArchiveFactory().get_archive()
    if queue.podposts[0] == id:
        pyotherside.send("stopping")
    queue.remove(id)
    archive.insert(id)

    get_queue_posts()


def queue_top_to_archive():
    """
    Remove top Element from queue
    """

    queue = QueueFactory().get_queue()
    queue_to_archive(queue.podposts[0])
    queue_play()


def queue_do_download(id):
    """
    Do the Download of an archive
    """

    queue = QueueFactory().get_queue()
    for perc in queue.download(id):
        pyotherside.send("downloading", id, perc)


def queue_download_all():
    """
    Download the whole queue if not done
    """

    queue = QueueFactory().get_queue()
    queue.download_all()


def queue_hr_time(position):
    """
    Get the human readable time of a podpost
    """

    queue = QueueFactory().get_queue()
    if len(queue.podposts) > 0:
        pyotherside.send(
            "hrtime", queue.podposts[0], queue.get_hr_time(position)
        )


def set_duration(duration):
    """
    Set podposts duration if not set
    """

    QueueFactory().get_queue().set_duration(duration)
    get_queue_posts()


def get_episode_chapters(id):
    """
    Sends the list of chapters for an episode
    id: id of the episode
    """

    entry = Factory().get_podpost(id)
    chapters = entry.get_chapters()
    pyotherside.send("episodeChapters", chapters)


def send_first_episode_chapters(id):
    """
    Sends episode data and chapter list
    (necessary when chapters of the currently
    playing episode are selected/deselected)
    """
    entry = Factory().get_podpost(id)
    edata = entry.get_data()
    chapters = entry.get_chapters()
    pyotherside.send("setFirst", edata, chapters)


def toggle_chapter(episodeid, chapterid):
    """
    Toggles the selected state of a chapterid
    episodeid: id of the episode
    chapterid: chapter number
    """
    entry = Factory().get_podpost(episodeid)
    entry.toggle_chapter(chapterid)


class QueueHandler:
    def __init__(self):
        self.bgthread = threading.Thread()
        self.bgthread.start()
        self.bgthread1 = threading.Thread()
        self.bgthread1.start()
        pyotherside.atexit(self.doexit)

    def doexit(self):
        """
        On exit: we need to stop ourself
        """

        queue_stop(-1)

    def getqueueposts(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_queue_posts)
        self.bgthread.start()

    def getfirstentry(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_first_entry)
        self.bgthread.start()

    def queuedownload(self, id):
        """
        download audio post
        """

        dlthread = threading.Thread(target=queue_do_download, args=[id])
        dlthread.start()

    def queueinserttop(self, id):
        if self.bgthread1.is_alive():
            return
        self.bgthread1 = threading.Thread(target=queue_insert_top, args=[id])
        self.bgthread1.start()

    def queueinsertnext(self, id):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=queue_insert_next, args=[id])
        self.bgthread.start()

    def queueinsertbottom(self, id):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=queue_insert_bottom, args=[id])
        self.bgthread.start()

    def queuemoveup(self, id):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=queue_move_up, args=[id])
        self.bgthread.start()

    def queuemovedown(self, id):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=queue_move_down, args=[id])
        self.bgthread.start()

    def queueplay(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=queue_play)
        self.bgthread.start()

    def queuepause(self, position):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=queue_pause, args=[position])
        self.bgthread.start()

    def queuestop(self, position):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=queue_stop, args=[position])
        self.bgthread.start()

    def queueseek(self, position):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=queue_seek, args=[position])
        self.bgthread.start()

    def queuehrtime(self, position):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=queue_hr_time, args=[position])
        self.bgthread.start()

    def queuetoarchive(self, id):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=queue_to_archive, args=[id])
        self.bgthread.start()

    def queuetoptoarchive(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=queue_top_to_archive)
        self.bgthread.start()

    def setduration(self, duration):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=set_duration, args=[duration])
        self.bgthread.start()

    def getepisodechapters(self, episodeid):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_episode_chapters, args=[episodeid])
        self.bgthread.start()

    def togglechapter(self, episodeid, chapterid):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=toggle_chapter, args=[episodeid, chapterid])
        self.bgthread.start()

queuehandler = QueueHandler()
