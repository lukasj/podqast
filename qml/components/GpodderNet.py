#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pyotherside
import threading
import sys

sys.path.append("/usr/share/harbour-podqast/python")

from mygpoclient import public

tagslist = []
toplist = []
client = public.PublicClient()


def get_toptags():
    """
    Get the Gpodder top tag list
    """

    if tagslist == []:
        toptags = client.get_toptags()
        for tag in toptags:
            if tag.usage > 70:
                tagslist.append({"tagname": tag.tag, "position": "TagsList"})
    pyotherside.send("toptags", tagslist)


def get_toplist():
    """
    Get the actual top podcasts (used for Discover grid)
    """

    if toplist == []:
        tops = client.get_toplist(count=12)
        for top in tops:
            description = top.description
            if len(description) > 160:
                description = description[:160] + "..."

            toplist.append(
                {
                    "title": top.title,
                    "url": top.url,
                    "logo_url": top.logo_url,
                    "description": description,
                }
            )
    pyotherside.send("toplist", toplist)


def get_tags_by_name(tagname):
    """
    Get podcast list by tag name
    """
    pclist = []
    podcasts = client.get_podcasts_of_a_tag(tagname, count=20)
    for podcast in podcasts:
        title = podcast.title
        if len(title) > 50:
            title = title[:50] + "..."
        description = podcast.description
        if len(description) > 160:
            description = description[:160] + "..."
        pclist.append(
            {
                "url": podcast.url,
                "title": title,
                "titlefull": podcast.title,
                "description": description,
                "website": podcast.website,
                "logo_url": podcast.logo_url,
            }
        )
    pyotherside.send("podcastlist", pclist)


def search_pods(query):
    """
    Search for podcast list
    query: the search query
    """

    pclist = []
    podcasts = client.search_podcasts(query)
    for podcast in podcasts:
        title = podcast.title
        if len(title) > 50:
            title = title[:50] + "..."
        description = podcast.description
        if len(description) > 160:
            description = description[:160] + "..."
        if not podcast.logo_url:
            logo_url = ""
        else:
            logo_url = podcast.logo_url

        pclist.append(
            {
                "url": podcast.url,
                "title": title,
                "titlefull": podcast.title,
                "description": description,
                "website": podcast.website,
                "logo_url": logo_url,
            }
        )
    pyotherside.send("podsearch", pclist)


class GpodderNet:
    def __init__(self):
        self.bgthread = threading.Thread()
        self.bgthread.start()

    def gettags(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_toptags)
        self.bgthread.start()

    def getpcbytagname(self, pcname):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(
            target=get_tags_by_name, args=[pcname]
        )
        self.bgthread.start()

    def gettoplist(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_toplist)
        self.bgthread.start()

    def searchpods(self, query):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=search_pods, args=[query])
        self.bgthread.start()


gpoddernet = GpodderNet()
