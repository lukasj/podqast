import QtQuick 2.0
import Sailfish.Silica 1.0

PullDownMenu {
    property string thispage
    busy: mainmenulab.text != ""

    MenuItem {
        text: qsTr("Discover")
        onClicked: {
            pageStack.clear()
            pageStack.replace(Qt.resolvedUrl("../pages/Discover.qml"), {}, PageStackAction.Immediate)
        }
        visible: thispage != "Discover"
    }
    MenuItem {
        text: qsTr("Library")
        onClicked: {
            pageStack.clear()
            pageStack.replace(Qt.resolvedUrl("../pages/Archive.qml"), {}, PageStackAction.Immediate)
        }
        visible: thispage != "Archive"
    }
    MenuItem {
        text: qsTr("Inbox")
        onClicked: {
            pageStack.clear()
            pageStack.replace(Qt.resolvedUrl("../pages/Inbox.qml"), {}, PageStackAction.Immediate)
        }
        visible: thispage != "Inbox"
    }
    MenuItem {
        text: qsTr("Playlist")
        onClicked: {
            pageStack.clear()
            pageStack.replace(Qt.resolvedUrl("../pages/Queue.qml"), {}, PageStackAction.Immediate)
        }
        visible: thispage != "Queue"
    }
    MenuLabel {
        id: mainmenulab
        visible: text != ""
        text: ""
        Connections {
            target: feedparserhandler
            onRefreshPost: {
                console.log("refreshPost")
                mainmenulab.text = posttitle ? qsTr("refreshing: ") + posttitle : ""
            }
        }
    }
}
