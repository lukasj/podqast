import QtQuick 2.0
import Sailfish.Silica 1.0

IconButton {
    property alias text: lbl.text

    Label {
        id: lbl
        opacity: parent.down
        Behavior on opacity {
            FadeAnimation {}
        }

        anchors {
            verticalCenter: parent.top
            horizontalCenter: parent.horizontalCenter
        }

        color: Theme.highlightColor
        font.pixelSize: Theme.fontSizeTiny
    }
}
