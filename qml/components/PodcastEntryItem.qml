import QtQuick 2.0
import Sailfish.Silica 1.0

ListItem {
    width: ListView.view.width
    contentHeight: Theme.itemSizeExtraLarge * 1.1
    onClicked: pageStack.push(Qt.resolvedUrl("../pages/PostDescription.qml"), { title: title, detail: detail,
                              length: length, date: fdate, duration: duration, href: link })

    onPressAndHold: console.log("press and hold")

    menu: IconContextMenu {
        id: contextMenu
        IconMenuItem {
            text: 'Play'
            icon.source: 'image://theme/icon-m-play'
            onClicked: {
                queuehandler.queueInsertTop(id);
                closeMenu();
                playerHandler.playicon=logo_url;
                playerHandler.playtext=title;
            }
        }
        IconMenuItem {
            text: 'QueueTop'
            icon.source: 'image://theme/icon-m-up'
            onClicked: {
                queuehandler.queueInsertNext(id);
                closeMenu()
            }
        }
        IconMenuItem {
            text: 'QueueBottom'
            icon.source: 'image://theme/icon-m-down'
            onClicked: {
                queuehandler.queueInsertBottom(id);
                closeMenu()
            }
        }
        IconMenuItem {
            text: 'Archive'
            icon.source: 'image://theme/icon-m-backup'
            onClicked: {
                queuehandler.queueToArchive(id); closeMenu()
            }
        }
        IconMenuItem {
            id: chapterselection
            text: qsTr("Select chapters")
            icon.source: 'image://theme/icon-m-menu'
            opacity: (haschapters) ? 1 : 0.3
            enabled: (haschapters)
            onClicked: {
                pageStack.push(Qt.resolvedUrl("../pages/Chapters.qml"), { episodeid: id })
            }
        }
    }

    Column {
        id: eventitem
        anchors.left: parent.left
        anchors.right: listarrow.left
        height: parent.height
        clip: true

        Label {
            width: parent.width
            text: title
            font.pixelSize: Theme.fontSizeExtraSmall
            font.bold: true
            wrapMode: Text.WordWrap
            padding: 5
        }
        Label {
            text: timestring == "" ? description : timestring + " · " + description
            width: parent.width
            font.pixelSize: Theme.fontSizeTiny
            wrapMode: Text.WordWrap
            textFormat: Text.StyledText
            padding: 5
        }
    }
    IconButton {
        id: listarrow
        height: parent.height
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        icon.source: "image://theme/icon-m-right"
        onClicked: {
            // feedparserhandler.renderHtml(description)
            pageStack.push(Qt.resolvedUrl("../pages/PostDescription.qml"), { title: title, detail: detail,
                               length: length, date: fdate, duration: duration, href: link })
        }
    }

}
